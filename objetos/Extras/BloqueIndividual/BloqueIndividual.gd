extends KinematicBody2D

func get_nombre_segun_i(i):
	if i == 0: return "Up"
	if i == 1: return "Down"
	if i == 2: return "Left"
	return "Right"

func _ready():
	set_collision_mask_bit(0,0)
	set_collision_layer_bit(0,1)
	set_collision_layer_bit(1,1)
	set_collision_layer_bit(2,1)
	set_collision_layer_bit(3,1)
	var i = 0
	for c in get_children():
		c.set_meta("tc",get_nombre_segun_i(i))
		i += 1
