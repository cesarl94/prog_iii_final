extends Area2D

export(int) var NUM = 0
var Activo = false

func _ready():
	set_meta("tc","boton")
	set_meta("p",self)
	

func _press():
	if $animacion.is_playing(): return
	$animacion.play("Press")
	Handle.sound.play_sound("switch")
	if Activo: return
	Activo = true
	
	var exito = false
	for c in get_children():
		if not c is Sprite and not c is CollisionShape2D and not c is AnimationPlayer:
			exito = true
			c._parent_button_press()
	if not exito:
		get_parent()._parent_button_press()

func _on_animation_finished(anim_name):
	Handle.sound.play_sound("switch")
	$Sprite.frame = 0


func _on_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "flecha": return
	body.get_meta("p")._activar()
	_press()
