extends Sprite

func esta_dentro(aabb,pos):
	return pos.x >= aabb.position.x and pos.x < aabb.position.x + aabb.size.x and pos.y >= aabb.position.y and pos.y < aabb.position.y + aabb.size.y

var comerciando = false

func _ready():
	_updateMiniatures()
	Handle.tablero = self
	set_process_input(false)

func _reset():
	$Dave.frame = 0
	$Skyler.frame = 1
	$Eddy.frame = 1
	$Dave._reset()
	$Skyler._reset()
	$Eddy._reset()
	_updateMiniatures()

func _updateMiniatures():
	match Handle.vikingoSN:
		0: 
			if $Dave.frame != 2: $Dave.frame = 0
			if $Skyler.frame != 2: $Skyler.frame = 1
			if $Eddy.frame != 2: $Eddy.frame = 1
		1:
			if $Dave.frame != 2: $Dave.frame = 1
			if $Skyler.frame != 2: $Skyler.frame = 0
			if $Eddy.frame != 2: $Eddy.frame = 1
		2:
			if $Dave.frame != 2: $Dave.frame = 1
			if $Skyler.frame != 2: $Skyler.frame = 1
			if $Eddy.frame != 2: $Eddy.frame = 0
			

#devuelve true si se metió, false si no
func _meterItem(vikingoName, numItem):
	if not has_node(vikingoName): return false
	var vikingo = get_node(vikingoName)
	if not vikingo._tiene_lugar(): return false
	vikingo._meter(numItem)
	return true


enum StateComercio{
	Idle, Move
}

var miniVikingoSelec
var miniVikingoMoveSN
var miniVikingoMove
var itemMove
var state

func get_mini_vikingo_selec():
	match miniVikingoMoveSN:
		0: 
			var vikingo = get_node("Dave")
			if vikingo.frame != 2: return vikingo
			else: return null
		1: 
			var vikingo = get_node("Skyler")
			if vikingo.frame != 2: return vikingo
			else: return null
		2: 
			var vikingo = get_node("Eddy")
			if vikingo.frame != 2: return vikingo
			else: return null
	return null

func _move_to_right():
	while(true):
		miniVikingoMoveSN += 1
		if miniVikingoMoveSN > 3:
			miniVikingoMoveSN = 0
		if miniVikingoMoveSN == 3:
			miniVikingoMove = null
			return
		else:
			miniVikingoMove = get_mini_vikingo_selec()
			if miniVikingoMove != null: return
	

func _move_to_left():
	while(true):
		miniVikingoMoveSN -= 1
		if miniVikingoMoveSN < 0:
			miniVikingoMoveSN = 3
		if miniVikingoMoveSN == 3:
			miniVikingoMove = null
			return
		else:
			miniVikingoMove = get_mini_vikingo_selec()
			if miniVikingoMove != null: return


func _iniciar_comercio(posicion_pantalla, numero_item):
	pause_mode = Node.PAUSE_MODE_PROCESS
	get_tree().paused = true
	miniVikingoMoveSN = Handle.vikingoSN
	miniVikingoSelec = get_mini_vikingo_selec()
	state = StateComercio.Idle
	Handle.sound.play_sound("menu_enter")
	Handle.item.show()
	Handle.item.frame = numero_item
	Handle.item.position = posicion_pantalla
	for v in Handle.vikingos.get_children():
		v.visible = true
	comerciando = true
	
func get_world_position(event_position):
	var _global_position = Handle.camara.get_camera_screen_center() * 2
	var pos_event = event_position
	pos_event.y -= 96
	var catetos = Vector2(320, 176) - pos_event
	_global_position -= catetos
	return _global_position / 2

func get_screen_position(world_position):
	var screen_center = Handle.camara.get_camera_screen_center()
	var catetos = screen_center - world_position
	return Vector2(320, 272) - catetos * 2

func get_romboidal_distance(punto1, punto2):
	return abs(punto1.x - punto2.x) + abs(punto1.y - punto2.y)

func get_vikingo_mas_cercano(punto):
	var menor_distancia = -1.0
	var vikingo
	for v in Handle.vikingos.get_children():
		var distancia = get_romboidal_distance(v.global_position,punto)
		if menor_distancia < 0 or distancia < menor_distancia:
			menor_distancia = distancia
			vikingo = v
	if menor_distancia < 20 and Handle.distancia(Handle.vikingoSelec.global_position, vikingo.global_position) < 60:
		if vikingo != Handle.vikingoSelec and not get_node(vikingo.name)._tiene_lugar(): return null
		return vikingo
	return null

var last_vmc = null
var tacho = false

func _input(event):
	if event is InputEventScreenDrag:
		
		if event.position.y <= 96:
			Handle.item.position = event.position
			var dentro = null
			var tacho_antes = tacho
			tacho = false
			for v in Handle.vikingos.get_children():
				var miniv = get_node(v.name)
				if esta_dentro(miniv.aabb, event.position) and Handle.distancia(Handle.vikingoSelec.global_position, v.global_position) < 60:
					dentro = miniv
			if esta_dentro(AABB(Vector3(514, 32, 0),Vector3(30, 32, 0)),event.position):
				tacho = true
			
			if not tacho:
				if dentro != null:
					var vikingo = Handle.vikingos.get_node(dentro.name)
					
					if vikingo != last_vmc:
						if last_vmc != null and last_vmc != Handle.vikingoSelec:
							get_node(last_vmc.name)._despresentar()
						
						last_vmc = vikingo
						
						if last_vmc != null:
							Handle.item.position = get_screen_position(last_vmc.global_position)
							
							
							if last_vmc != Handle.vikingoSelec:
								var mini = get_node(last_vmc.name)
								mini._presentar(Handle.item.frame)
								Handle.sound.play_sound("globo")
				else:
					if last_vmc != null and last_vmc != Handle.vikingoSelec:
							get_node(last_vmc.name)._despresentar()
						
					last_vmc = null
					
			else:
				Handle.item.position = Vector2(529, 48)
			if tacho != tacho_antes:
				var miniv = null
				if last_vmc != null and last_vmc != Handle.vikingoSelec:
					miniv = get_node(last_vmc.name)
				
				if tacho:
					Handle.sound.play_sound("globo")
					if miniv != null:
						miniv._despresentar()
				elif miniv != null:
					miniv._presentar(Handle.item.frame)
			
		else:
			var world_position = get_world_position(event.position)
			var vikingo_mas_cercano = get_vikingo_mas_cercano(world_position)
			if vikingo_mas_cercano != last_vmc:
				if last_vmc != null and last_vmc != Handle.vikingoSelec:
					get_node(last_vmc.name)._despresentar()
				
				
				last_vmc = vikingo_mas_cercano
				if last_vmc != null:
					
					Handle.item.position = get_screen_position(last_vmc.global_position)
					Handle.sound.play_sound("globo")
					if last_vmc != Handle.vikingoSelec:
						var mini = get_node(last_vmc.name)
						mini._presentar(Handle.item.frame)
			
			if last_vmc == null:
				Handle.item.position = event.position
			
	if event is InputEventScreenTouch:
		if not event.pressed:
			_terminar_comercio()
			if tacho:
				get_node(Handle.vikingoSelec.name)._despresentar()
				Handle.sound.play_sound("cae_altura")
				
				
			else:
				if last_vmc == null:
					Handle.sound.play_sound("no")
					get_node(Handle.vikingoSelec.name)._meter_presentado(false)
				elif last_vmc == Handle.vikingoSelec:
					get_node(Handle.vikingoSelec.name)._meter_presentado(false)
					Input.action_press("Y")
				else:
					var selec = get_node(Handle.vikingoSelec.name)
					var otro = get_node(last_vmc.name)
					otro._meter_presentado()
					selec._despresentar()



func _terminar_comercio():
	pause_mode = Node.PAUSE_MODE_INHERIT
	comerciando = false
	get_tree().paused = false
	Handle.item.hide()

