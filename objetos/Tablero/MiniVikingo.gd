extends Sprite

export(PackedScene) var HPscene = null
export(PackedScene) var Itemscene = null

var HP = 3
var ExtraVida = false
var items = []
var itemSelecPos #0 arr izq, 1 arr der, 2 aba izq, 3 aba der

func _ready():
	if Engine.editor_hint: return
	self.texture = texture
	if HPscene == null or texture == null or Itemscene == null:
		get_tree().quit()
		return
	hframes = 3
	frame = 1
	actualizar_HP()
	itemSelecPos = 0
	actualizarSelec()
	
	for i in 4:
		var item = Itemscene.instance()
		item.frame = 0
		match i:
			0: item.position = Vector2(17, -13)
			1: item.position = Vector2(33, -13)
			2: item.position = Vector2(17, 3)
			3: item.position = Vector2(33, 3)
		items.push_back(item)
		add_child(item)

func actualizar_HP():
	while($HPpoints.get_children().size() < HP):
		var newHP = HPscene.instance()
		newHP.position.x = $HPpoints.get_children().size() * 8
		$HPpoints.add_child(newHP)
		
	while($HPpoints.get_children().size() > HP and HP >= 0):
		var ultimoHP = $HPpoints.get_children().back()
		ultimoHP.queue_free()
		$HPpoints.remove_child(ultimoHP)
	
	if ExtraVida and not $Extra.is_visible_in_tree():
		$Extra.show()
	elif not ExtraVida and $Extra.is_visible_in_tree():
		$Extra.hide()

#devuelve si el señalador era visible
func actualizarSelec():
	var es_visible = $Selec.is_visible_in_tree()
	$Selec.show()
	match itemSelecPos:
		0: $Selec.position = Vector2(17, -13)
		1: $Selec.position = Vector2(33, -13)
		2: $Selec.position = Vector2(17, 3)
		3: $Selec.position = Vector2(33, 3)
	return es_visible

func _tiene_lugar():
	for i in 4:
		if items[i].frame == 0:
			return true
	return false

func _esta_vacio():
	for i in 4:
		if items[i].frame != 0:
			return false
	return true

func _meter(numeroItem):
	for i in 4:
		if items[i].frame == 0:
			items[i].frame = numeroItem
			Handle.sound.play_sound("agarra_item")
			if items[itemSelecPos].frame == 0:
				itemSelecPos = i
				actualizarSelec()
			return

func _vaciar_inventario():
	for i in 4:
		items[i].frame = 0

#devuelve true si sigue viviendo, false si se murió
func _vidaSuma(cantidad):
	if cantidad < 0:
		if ExtraVida:
			ExtraVida = false
			cantidad+= 1
	HP += cantidad
	if HP > 3:
		HP = 3
	actualizar_HP()
	
	if HP > 0: return true
	return false

func _agregar_extra():
	if ExtraVida: return false
	ExtraVida = true
	actualizar_HP()
	return true

func _reset():
	HP = 3
	ExtraVida = false
	for i in 4:
		items[i].frame = 0
	itemSelecPos = 0 
	actualizar_HP()
	actualizarSelec()
	

func _get_objeto_selec():
	return items[itemSelecPos].frame

func _presentar_seleccionado():
	#$Presentar.add_child(items[itemSelecPos].duplicate())
	$Presentar.frame = items[itemSelecPos].frame
	$Presentar.position = $Selec.position
	items[itemSelecPos].frame = 0
	#$Presentar.get_children().front().hide()
	

#devuelve false si no tiene lugar
func _presentar(num):
	$Presentar.frame = num
	var lugar_libre = -1
	for i in 4:
		if items[i].frame == 0:
			lugar_libre = i
			break
	if lugar_libre == -1: return false
	$Presentar.position = items[lugar_libre].position
	$Presentar.show()
	return true

func _meter_presentado():
	var lugar_libre = -1
	for i in 4:
		if items[i].frame == 0:
			lugar_libre = i
			break
	if lugar_libre == -1: return false
	
	$Presentar.hide()
	_meter($Presentar.frame)
	if items[itemSelecPos].frame == 0:
		itemSelecPos = lugar_libre
		actualizarSelec()
	














