extends Sprite

export(PackedScene) var HPscene = null
export(PackedScene) var Itemscene = null

var HP = 3
var ExtraVida = false
var items = []
var itemSelecPos #0 arr izq, 1 arr der, 2 aba izq, 3 aba der
var aabb = AABB(Vector3(),Vector3(65*2, 32*2, 0))

signal item

func _ready():
	if Engine.editor_hint: return
	set_process_input(false)
	self.texture = texture
	if HPscene == null or texture == null or Itemscene == null:
		get_tree().quit()
		return
	hframes = 3
	frame = 1
	actualizar_HP()
	itemSelecPos = 0
	
	for i in 4:
		var item = Itemscene.instance()
		item.frame = 0
		match i:
			0: item.position = Vector2(17, -13)
			1: item.position = Vector2(33, -13)
			2: item.position = Vector2(17, 3)
			3: item.position = Vector2(33, 3)
		items.push_back(item)
		add_child(item)
	aabb.position.x = position.x * 2 + 288
	aabb.position.y = position.y * 2 + 22

func actualizar_HP():
	while($HPpoints.get_children().size() < HP):
		var newHP = HPscene.instance()
		newHP.position.x = $HPpoints.get_children().size() * 8
		$HPpoints.add_child(newHP)
		
	while($HPpoints.get_children().size() > HP and HP >= 0):
		var ultimoHP = $HPpoints.get_children().back()
		ultimoHP.queue_free()
		$HPpoints.remove_child(ultimoHP)
	
	if ExtraVida and not $Extra.is_visible_in_tree():
		$Extra.show()
	elif not ExtraVida and $Extra.is_visible_in_tree():
		$Extra.hide()



func _tiene_lugar():
	for i in 4:
		if items[i].frame == 0:
			return true
	return false

func _esta_vacio():
	for i in 4:
		if items[i].frame != 0:
			return false
	return true

func _meter(numeroItem, play_sound = true):
	for i in 4:
		if items[i].frame == 0:
			items[i].frame = numeroItem
			if play_sound:
				Handle.sound.play_sound("agarra_item")
			if items[itemSelecPos].frame == 0:
				itemSelecPos = i
			return

func _vaciar_inventario():
	for i in 4:
		items[i].frame = 0

#devuelve true si sigue viviendo, false si se murió
func _vidaSuma(cantidad):
	if cantidad < 0:
		if ExtraVida:
			ExtraVida = false
			cantidad+= 1
	HP += cantidad
	if HP > 3:
		HP = 3
	actualizar_HP()
	
	if HP > 0: return true
	return false

func _agregar_extra():
	if ExtraVida: return false
	ExtraVida = true
	actualizar_HP()
	return true

func _reset():
	HP = 3
	ExtraVida = false
	for i in 4:
		items[i].frame = 0
	itemSelecPos = 0 
	actualizar_HP()
	

func _get_objeto_selec():
	return items[itemSelecPos].frame

#devuelve false si no tiene lugar
func _presentar(num):
	$Presentar.frame = num
	var lugar_libre = -1
	for i in 4:
		if items[i].frame == 0:
			lugar_libre = i
			break
	if lugar_libre == -1: return false
	$Presentar.position = items[lugar_libre].position
	$Presentar.show()
	$Timer.start()
	return true

func _despresentar():
	$Presentar.hide()
	$Timer.stop()

func _meter_presentado(play_sound = true):
	var lugar_libre = -1
	for i in 4:
		if items[i].frame == 0:
			lugar_libre = i
			break
	if lugar_libre == -1: return false
	
	$Presentar.hide()
	$Timer.stop()
	_meter($Presentar.frame, play_sound)
	if items[itemSelecPos].frame == 0:
		itemSelecPos = lugar_libre

var selec_PJ = false
var selec_item = false



func _input(event):
	if event is InputEventScreenDrag:
		if selec_PJ:
			selec_PJ = false
			set_process_input(false)
		if selec_item:
			selec_item = false
			set_process_input(false)
			get_parent()._iniciar_comercio(event.position, items[itemSelecPos].frame)
			var aux = items[itemSelecPos].frame
			items[itemSelecPos].frame = 0
			_presentar(aux)
			




func _on_TouchPJ_pressed():
	selec_PJ = true
	set_process_input(true)


func _on_TouchPJ_released():
	if selec_PJ:
		Input.action_press("NUM")
		match name:
			"Dave": Input.action_press("UNO")
			"Skyler": Input.action_press("DOS")
			"Eddy": Input.action_press("TRES")
	selec_PJ = false
	set_process_input(false)

func press_item(presiona, numero):
	if not Handle.vikingos.get_node(name) == Handle.vikingoSelec:
		_on_TouchPJ_pressed()
		_on_TouchPJ_released()
	if items[numero].frame == 0:
		return
	
	
	if not presiona:
		if not selec_item:
			return
		else:
			Input.action_press("Y")
	set_process_input(presiona)
	itemSelecPos = numero
	selec_item = presiona


func _on_Timer_timeout():
	$Presentar.visible = not $Presentar.visible
