extends Sprite

func _ready():
	_updateMiniatures()
	Handle.tablero = self
	set_process(false)

func _reset():
	$Dave.frame = 0
	$Skyler.frame = 1
	$Eddy.frame = 1
	$Dave._reset()
	$Skyler._reset()
	$Eddy._reset()
	_updateMiniatures()

func _updateMiniatures():
	match Handle.vikingoSN:
		0: 
			if $Dave.frame != 2: $Dave.frame = 0
			if $Skyler.frame != 2: $Skyler.frame = 1
			if $Eddy.frame != 2: $Eddy.frame = 1
		1:
			if $Dave.frame != 2: $Dave.frame = 1
			if $Skyler.frame != 2: $Skyler.frame = 0
			if $Eddy.frame != 2: $Eddy.frame = 1
		2:
			if $Dave.frame != 2: $Dave.frame = 1
			if $Skyler.frame != 2: $Skyler.frame = 1
			if $Eddy.frame != 2: $Eddy.frame = 0
			

#devuelve true si se metió, false si no
func _meterItem(vikingoName, numItem):
	if not has_node(vikingoName): return false
	var vikingo = get_node(vikingoName)
	if not vikingo._tiene_lugar(): return false
	vikingo._meter(numItem)
	return true


enum StateComercio{
	Idle, Move
}

var miniVikingoSelec
var miniVikingoMoveSN
var miniVikingoMove
var itemMove
var state

func get_mini_vikingo_selec():
	match miniVikingoMoveSN:
		0: 
			var vikingo = get_node("Dave")
			if vikingo.frame != 2: return vikingo
			else: return null
		1: 
			var vikingo = get_node("Skyler")
			if vikingo.frame != 2: return vikingo
			else: return null
		2: 
			var vikingo = get_node("Eddy")
			if vikingo.frame != 2: return vikingo
			else: return null
	return null

func _move_to_right():
	while(true):
		miniVikingoMoveSN += 1
		if miniVikingoMoveSN > 3:
			miniVikingoMoveSN = 0
		if miniVikingoMoveSN == 3:
			miniVikingoMove = null
			return
		else:
			miniVikingoMove = get_mini_vikingo_selec()
			if miniVikingoMove != null: return
	

func _move_to_left():
	while(true):
		miniVikingoMoveSN -= 1
		if miniVikingoMoveSN < 0:
			miniVikingoMoveSN = 3
		if miniVikingoMoveSN == 3:
			miniVikingoMove = null
			return
		else:
			miniVikingoMove = get_mini_vikingo_selec()
			if miniVikingoMove != null: return


func _iniciar_comercio():
	pause_mode =Node.PAUSE_MODE_PROCESS
	get_tree().paused = true
	set_process(true)
	miniVikingoMoveSN = Handle.vikingoSN
	miniVikingoSelec = get_mini_vikingo_selec()
	$Timer.start()
	state = StateComercio.Idle
	Handle.sound.play_sound("menu_enter")

func _terminar_comercio():
	pause_mode = Node.PAUSE_MODE_INHERIT
	get_tree().paused = false
	set_process(false)
	$Timer.stop()
	miniVikingoSelec.get_node("Selec").show()

func _get_vikingo(miniVikingo):
	return Handle.vikingos.get_node(miniVikingo.name)

func _distancia_entre_dos_mini_vikingos(miniVikingo1, miniVikingo2):
	if miniVikingo1 == miniVikingo2: return 0.0
	var vikingo1 = _get_vikingo(miniVikingo1)
	var vikingo2 = _get_vikingo(miniVikingo2)
	return Handle.distancia(vikingo1.position, vikingo2.position)

func _process(delta):
	match state:
		StateComercio.Idle:
			if Input.is_action_just_pressed("LEFT"):
				if miniVikingoSelec.itemSelecPos % 2 == 1:
					miniVikingoSelec.itemSelecPos -= 1
					if not miniVikingoSelec.actualizarSelec():
						$Timer.start()
			if Input.is_action_just_pressed("RIGHT"):
				if miniVikingoSelec.itemSelecPos % 2 == 0:
					miniVikingoSelec.itemSelecPos += 1
					if not miniVikingoSelec.actualizarSelec():
						$Timer.start()
			if Input.is_action_just_pressed("DOWN"):
				if miniVikingoSelec.itemSelecPos < 2:
					miniVikingoSelec.itemSelecPos += 2
					if not miniVikingoSelec.actualizarSelec():
						$Timer.start()
			if Input.is_action_just_pressed("UP"):
				if miniVikingoSelec.itemSelecPos > 1:
					miniVikingoSelec.itemSelecPos -= 2
					if not miniVikingoSelec.actualizarSelec():
						$Timer.start()
			if Input.is_action_just_pressed("TAB"):
				Input.action_release("TAB")
				_terminar_comercio()
			if Input.is_action_just_pressed("B"):
				itemMove = miniVikingoSelec._get_objeto_selec()
				if itemMove != 0:
					miniVikingoSelec.get_node("Selec").show()
					state = StateComercio.Move
					miniVikingoSelec._presentar_seleccionado()
					miniVikingoMoveSN = Handle.vikingoSN
					miniVikingoMove = miniVikingoSelec
					$Timer.start()
				
			if Input.is_action_just_pressed("NUM"):
				miniVikingoSelec.get_node("Selec").show()
				miniVikingoMoveSN = Handle.vikingoSN
				miniVikingoSelec = get_mini_vikingo_selec()
				$Timer.start()
			if Input.is_action_just_pressed("Y"):
				_terminar_comercio()
				Handle._use_object()
				Input.action_release("Y")
				
		StateComercio.Move:
			if Input.is_action_just_pressed("LEFT") or Input.is_action_just_pressed("RIGHT"):
				var itemPresentado
				if miniVikingoMoveSN == 3:
					itemPresentado = $Item
				else:
					itemPresentado = miniVikingoMove.get_node("Presentar")
				itemPresentado.hide()
				$Timer.start()
				
				while(true):
					if Input.is_action_just_pressed("LEFT"): _move_to_left()
					else: _move_to_right()
					if miniVikingoMove == null:
						$Item.show()
						$Item.frame = itemMove
						break
					else:
						if _distancia_entre_dos_mini_vikingos(miniVikingoSelec, miniVikingoMove) < 60 and miniVikingoMove._presentar(itemMove):
							break
			if Input.is_action_just_pressed("Y"):
				Input.action_press("TAB")
			if Input.is_action_just_pressed("TAB"):
				Input.action_press("B")
				Input.action_release("TAB")
				_terminar_comercio()
			if Input.is_action_just_pressed("B"):
				Input.action_release("B")
				if miniVikingoMove == null:
					$Item.hide()
					state = StateComercio.Idle
					_selec_proximo()
				else:
					miniVikingoMove._meter_presentado()
					state = StateComercio.Idle
					_selec_proximo()
			if Input.is_action_just_pressed("Y"):
				Input.action_release("Y")
				Handle._use_object()
			
			
func _selec_proximo():
	if not miniVikingoSelec._esta_vacio():
		for i in 4:
			if miniVikingoSelec.items[i].frame != 0:
				miniVikingoSelec.itemSelecPos = i
				miniVikingoSelec.actualizarSelec()
				break

func _on_Timer_timeout():
	match state:
		StateComercio.Idle:
			var Selec = miniVikingoSelec.get_node("Selec")
			if Selec.is_visible_in_tree():
				Selec.hide()
			else: Selec.show()
		StateComercio.Move:
			var itemPresentado
			if miniVikingoMoveSN == 3:
				itemPresentado = $Item
			else:
				itemPresentado = miniVikingoMove.get_node("Presentar")
			if itemPresentado.is_visible_in_tree():
				itemPresentado.hide()
			else: itemPresentado.show()
			
	$Timer.start()
























