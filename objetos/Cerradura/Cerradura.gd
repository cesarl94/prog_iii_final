tool
extends Area2D

export(String,"Amarillo","Rojo","Azul") var color = "Amarillo" setget setcolor

onready var Handle = get_node("/root/Handle")

func setcolor(newVar):
	color = newVar
	if not Engine.editor_hint and not is_inside_tree(): return
	match color:
		"Amarillo": $Sprite.frame = 0
		"Rojo": $Sprite.frame = 1
		"Azul": $Sprite.frame = 2

func _ready():
	if Engine.editor_hint: return
	
	self.color = color
	set_meta("tc","cerr")
	set_meta("c",color)
	set_meta("p",self)

func _abrir():
	set_meta("tc",null)
	set_meta("c",null)
	set_meta("p",null)
	$Sprite.hide()
	Handle.sound.play_sound("llave")
	for c in get_children():
		if not c is CollisionShape2D and not c is Sprite:
			c._abrir()
