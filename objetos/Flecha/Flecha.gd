extends KinematicBody2D

export(PackedScene) var Chispa = null
var VELOCITY = 197.0

func _ready():
	set_meta("tc","flecha")
	set_meta("p",self)
	

var efectiva = true

func _physics_process(delta):
	if $Sprite.flip_h: move_and_slide(Vector2(VELOCITY, 0.0))
	else: move_and_slide(Vector2(-VELOCITY, 0.0))
	
	for n in get_slide_count():
		var shape = get_slide_collision(n).collider_shape
		if not shape.has_meta("tc"): continue
		match shape.get_meta("tc"):
			"Left", "Down", "Up", "Right":
				_destruir()

	
	if abs(position.x - Handle.camara.get_camera_screen_center().x) > 168:
		queue_free()

func _activar():
	$Sprite.hide()
	var chispa = Chispa.instance()
	Handle.actualLevel.add_child(chispa)
	chispa._play(position)
	queue_free()

func _destruir():
	Handle.sound.play_sound("flecha_choca")
	$animacion.play("Destruir")
	set_physics_process(false)

func _salir(skyler):
	$Sprite.flip_h = skyler.MIRA_DERECHA
	position = skyler.position

func _on_animation_finished(anim_name):
	queue_free()
