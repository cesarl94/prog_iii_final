extends Area2D

var Activo

func _comenzar():
	set_meta("tc","switch")
	set_meta("p",self)
	Activo = get_meta("m")._get_state()
	
	if Activo: $Sprite.frame = 1
	else: $Sprite.frame = 0

func _change():
	Handle.sound.play_sound("switch")
	Activo = !Activo
	if Activo:
		$Sprite.frame = 1
		get_meta("m")._play()
	else:
		$Sprite.frame = 0
		get_meta("m")._stop()
	get_parent().get_parent()._cambiaAntigravedad()
	

func _on_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "flecha": return
	body.get_meta("p")._activar()
	_change()
