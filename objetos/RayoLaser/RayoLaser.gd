extends Area2D

export(PackedScene) var Chispa = null
var VELOCITY = 132.62
var destruida = false
var entes_cercanos = []

func _ready():
	set_meta("tc","laser")
	set_meta("p",self)

func _physics_process(delta):
	if $Sprite.flip_h: position += Vector2(VELOCITY * delta, 0.0)
	else: position += Vector2(-VELOCITY * delta, 0.0)

func _salir(pos_sale, flipear):
	$Sprite.flip_h = flipear
	position = pos_sale

func _destruir():
	if destruida: return
	destruida = true
	z_index = -5
	$animacion.play("Destruir")
	set_physics_process(false)
	if entes_cercanos.find("cam") != -1:
		Handle.sound.play_sound("ataja")

func _chispaso():
	$Sprite.hide()
	var chispa = Chispa.instance()
	Handle.actualLevel.add_child(chispa)
	chispa._play(position + Vector2(0,3))
	queue_free()

func _on_animation_finished(anim_name):
	queue_free()

func _on_area_entered(area):
	if destruida or not area.has_meta("tc") or area.get_meta("tc") != "v": return
	area.get_meta("p")._danio(position, 1, 1)
	_chispaso()


func _on_body_entered(body):
	if destruida: return
	var shape = body.get_children().front()
	if not shape.has_meta("tc"): return
	var meta = shape.get_meta("tc")
	if meta == "Down" or meta == "Up" or meta == "Left" or meta == "Right":
		if entes_cercanos.find("cam") != -1:
			Handle.sound.play_sound("ataja")
		_destruir()

func _on_screen_area_entered(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "cam": return
	entes_cercanos.push_back(meta)


func _on_screen_area_exited(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "cam": return
	entes_cercanos.erase(meta)
	if entes_cercanos.empty():
		queue_free()
