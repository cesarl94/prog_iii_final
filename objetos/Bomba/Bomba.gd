extends KinematicBody2D

var motion = Vector2()
var objetosADestruir = []
var ascensor = null

func _ready():
	set_meta("tc","bomba")
	set_meta("p",self)

func _physics_process(delta):
	motion.y += delta * Handle.GRAVITY
	move_and_slide(motion)
	for n in get_slide_count():
		var shape = get_slide_collision(n).collider_shape
		if shape.has_meta("tc") and shape.get_meta("tc") == "Down":
			motion.y = 5.0

func _on_animation_finished(anim_name):
	$SpriteBomba.hide()
	$SpriteBoom.show()
	$Timer.start()
	Handle.shakeCameraBegin()
	Handle.sound.play_sound("bomba")
	for o in objetosADestruir:
		o._kill()
	if ascensor != null:
		ascensor.bombas.erase(self)
	set_physics_process(false)

func _on_Timer_timeout():
	queue_free()

func _on_area_entered(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "pc" and meta != "v" and meta != "cl" and meta != "barrera": return
	objetosADestruir.push_back(area.get_meta("p"))

func _on_area_exited(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "pc" and meta != "v" and meta != "cl" and meta != "barrera": return
	objetosADestruir.erase(area.get_meta("p"))

func _on_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "e": return
	objetosADestruir.push_back(body.get_meta("p"))

func _on_body_exited(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "e": return
	objetosADestruir.erase(body.get_meta("p"))
