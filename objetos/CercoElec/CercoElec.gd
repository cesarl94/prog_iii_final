tool
extends Area2D

export(bool) var HORIZONTAL = true setget cambiaHV
export(int) var LARGO = 4 setget cambiaL
export(bool) var APAGABLE = false setget cambiaA
var Encendido = true

onready var Handle = get_node("/root/Handle")

func cambiaHV(newValor):
	HORIZONTAL = newValor
	if not Engine.editor_hint and not is_inside_tree(): return
	if HORIZONTAL: $Sprite.rotation_degrees = 0
	else: $Sprite.rotation_degrees = 90
	$Sprite.rotation = deg2rad($Sprite.rotation_degrees)

func cambiaL(newValor):
	LARGO = newValor
	if not Engine.editor_hint and not is_inside_tree(): return
	$Sprite.region_rect = Rect2($Sprite.region_rect.position.x,$Sprite.region_rect.position.y,16 * LARGO, 8)

func cambiaA(newValor):
	APAGABLE = newValor
	if not Engine.editor_hint and not is_inside_tree(): return
	if APAGABLE: $Sprite.region_rect.position.y = 24
	else: $Sprite.region_rect.position.y = 0


func _ready():
	if Engine.editor_hint: return
	self.HORIZONTAL = HORIZONTAL
	self.LARGO = LARGO
	self.APAGABLE = APAGABLE	
	$Sprite.material.set_shader_param("tiempo_comienza_apagarse",0.0)
	$Sprite.material.set_shader_param("size_tile_Y",0.11111111111)
	z_index += -get_parent().z_index 

func _apagar():
	if $Sprite.region_rect.position.y == 0: return
	$Sprite.material.set_shader_param("tiempo_comienza_apagarse",Handle.totalTime)
	$Sprite.region_rect.position.y = 48
	$Timer.start()
	Encendido = false

func _parent_button_press():
	_apagar()

func _process(delta):
	if Engine.editor_hint: return
	$Sprite.material.set_shader_param("tiempo",Handle.totalTime)

func _on_Timer_timeout():
	queue_free()

func _on_area_entered(area):
	if not Encendido or not area.has_meta("tc") or area.get_meta("tc") != "v": return
	area.get_meta("p")._matar("Electrocucion")
	Handle.sound.play_sound("electrocucion")

func _on_body_entered(body):
	if not Encendido or not body.has_meta("tc") or body.get_meta("tc") != "e": return
	body.get_meta("p")._girar()






