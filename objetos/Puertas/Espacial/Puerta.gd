extends Node2D

export(bool) var AUTOMATICA
export(bool) var SE_CIERRA
var abierta = false
var entes = []

func _ready():
	$StaticBody2D/CollisionPolygon2D.set_meta("tc","Right")
	$StaticBody2D/CollisionPolygon2D2.set_meta("tc","Left")
	z_index += -get_parent().z_index

func _abrir():
	abierta = true
	$AnimationPlayer.play("Abrir")
	$StaticBody2D.set_collision_layer_bit(2, 0)
	$StaticBody2D.set_collision_layer_bit(3, 0)
	Handle.sound.play_sound("puerta_espacial")


func _cerrar():
	abierta = false
	$AnimationPlayer.play("Cerrar")
	$StaticBody2D.set_collision_layer_bit(2, 1)
	$StaticBody2D.set_collision_layer_bit(3, 1)
	Handle.sound.play_sound("puerta_espacial")

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	entes.push_back(area.get_meta("p"))
	if AUTOMATICA and not abierta:
		_abrir()
	

func _on_area_exited(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	entes.erase(area.get_meta("p"))
	if SE_CIERRA and abierta and entes.empty():
		_cerrar()

func _on_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "e": return
	entes.push_back(body.get_meta("p"))


func _on_body_exited(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "e": return
	entes.erase(body.get_meta("p"))
	if SE_CIERRA and abierta and entes.empty():
		_cerrar()
