extends Node2D

export(Array, Vector2) var puntosVa

var posEsta
var posVa
var state
var vikingos = []
var bombas = []

func _ready():
	$Area2D.set_meta("tc","ascensor")
	$Area2D.set_meta("p",self)
	#$Area2D.get_children().front().set_meta("tc","Down")
	state = Handle.Direccion.QUIETO
	posEsta = position
	posVa = Vector2(0.5, 0.5)

func _physics_process(delta):
	if state == Handle.Direccion.QUIETO:
		position = posEsta
	else:
		position.y += sign(posVa.y - posEsta.y) * delta * 60
		
		if state == Handle.Direccion.DOWN:
			for v in vikingos:
				if v.toca_suelo:
					v.position.y = position.y - 16
			for b in bombas:
				b.position.y = position.y - 12
		
		
		if (state == Handle.Direccion.DOWN and position.y >= posVa.y) or (state == Handle.Direccion.UP and position.y <= posVa.y):
			position = posVa
			posEsta = posVa
			posVa = Vector2(0.5, 0.5)
			
			if state == Handle.Direccion.DOWN:
				Handle.sound.stop_sound("ascensor_baja")
				Handle.sound.play_sound("ascensor_baja_llega")
			else:
				Handle.sound.stop_sound("ascensor_sube")
				Handle.sound.play_sound("ascensor_sube_llega")
			
			state = Handle.Direccion.QUIETO


func _subir():
	if state == Handle.Direccion.UP: return
	elif state == Handle.Direccion.DOWN:
		posEsta = posVa
		posVa = Vector2(0.5, 0.5)
		Handle.sound.stop_sound("ascensor_baja")
		
	var sube = false
	for n in puntosVa:
		if n != posEsta and n.y < posEsta.y:
			if posVa.y < 1.0 or posEsta.y - n.y < posEsta.y - posVa.y:
				posVa = n
				sube = true
	if sube:
		state = Handle.Direccion.UP
		Handle.sound.play_sound("ascensor_sube")
		Handle.sound.stop_sound("ascensor_sube_llega")

func _bajar():
	if state == Handle.Direccion.DOWN: return
	elif state == Handle.Direccion.UP:
		posEsta = posVa
		posVa = Vector2(0.5, 0.5)
		Handle.sound.stop_sound("ascensor_sube")
	
	var baja = false
	for n in puntosVa:
		if n != posEsta and n.y > posEsta.y:
			if posVa.y < 1.0 or n.y - posEsta.y < posVa.y - posEsta.y:
				posVa = n
				baja = true
	if baja:
		state = Handle.Direccion.DOWN
		Handle.sound.play_sound("ascensor_baja")
		Handle.sound.stop_sound("ascensor_baja_llega")

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	vikingos.push_back(area.get_meta("p"))

func _on_area_exited(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	vikingos.erase(area.get_meta("p"))

func _on_bomba_body_entered(bomba):
	bombas.push_back(bomba)
	bomba.ascensor = self

func _on_bomba_body_exited(bomba):
	bombas.erase(bomba)
	bomba.ascensor = null








