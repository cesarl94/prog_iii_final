extends Node2D

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	area.get_meta("p")._matar("trituracion")
	Handle.sound.play_sound("tritura")
