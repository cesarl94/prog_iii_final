extends Sprite

export(PackedScene) var Laser = null
var activo = false

func _ready():
	$Area2D.set_meta("tc","cl")#cañon laser
	$Area2D.set_meta("p",self)

func _on_animation_finished(anim_name):
	if not activo: return
	$AnimationPlayer.play("Dispara")
	_disparar()

func _disparar():
	if not activo or Handle.actualLevel == null: return
	var laser = Laser.instance()
	if flip_h: laser._salir(global_position + Vector2(10, -3), true)
	else: laser._salir(global_position + Vector2(-10, -3), false)
	Handle.actualLevel.add_child(laser)
	
	if Handle.distancia(global_position, Handle.camara.get_camera_screen_center()) < 183:
		Handle.sound.play_sound("canion_laser")
	

func _activar(valor):
	activo = valor
	if activo and not $AnimationPlayer.is_playing():
		$AnimationPlayer.play("Dispara")
		_disparar()

func _kill():
	get_parent().Escena_muerte = null
	get_parent()._death()