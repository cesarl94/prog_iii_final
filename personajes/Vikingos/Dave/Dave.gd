tool
extends KinematicBody2D

export(bool) var MIRA_DERECHA = true setget flipear
export(String, "El suelo","El aire","Caida libre") var COMIENZA_EN = "El suelo" setget set_comienza
export(PackedScene) var CHISPA1 = null
export(bool) var INMORTAL = false

onready var Handle = get_node("/root/Handle")

var MAX_RUN_SPEED = 130.1
var FUERZA_SALTO = 69.8
var FUERZA_SALTO_EXTRA_VELOCIDAD = 6.7
var MAX_EMPUJONES_SALTO = 5
var TIME_ENTRE_EMPUJON = 0.03

var state
var direccionX
var colisionOffset
var colision
var motion = Vector2()
var color = Color(0.84706, 0.0, 0.0)

var toca_suelo
var empujones_salto
var ultimoEmpujon
var tiempoCorriendo

var enascensor
var ignore_colision0
var vaparriba
var aterrizo#esta variable me salva de perder un nivel de salud al caer al comenzar
var empezar_a_levitar_terminada_animacion
var sentido_empuja
var antigrav = false
var restablecerUP = false

func set_comienza(valor):
	COMIENZA_EN = valor
	if not Engine.editor_hint and not is_inside_tree(): return
	_set_comienzo(valor)

func flipear(valor):
	MIRA_DERECHA = valor
	if not Engine.editor_hint and not is_inside_tree(): return
	$Sprite.flip_h = valor

func _set_comienzo(valor):
	match valor:
		"El suelo":
			$Sprite.frame = 0
			if not Engine.editor_hint:
				motion = Vector2()
				#aterrizo = true
				toca_suelo = true
				_set_state(Handle.State.Esperando)
		"El aire":
			$Sprite.frame = 21
			if not Engine.editor_hint:
				motion = Vector2()
				#aterrizo = true
				toca_suelo = false
				_set_state(Handle.State.Cayendo)
		"Caida libre":
			$Sprite.frame = 23
			if not Engine.editor_hint:
				motion = Vector2(0.0, 266.0)
				#aterrizo = false
				toca_suelo = false
				_set_state(Handle.State.Cayendo)





func _ready():
	if Engine.editor_hint: return
	colisionOffset = $colision.position
	
	self.MIRA_DERECHA = MIRA_DERECHA
	self.COMIENZA_EN = COMIENZA_EN
	
	direccionX = Handle.Direccion.QUIETO
	
	$Area2D.set_meta("tc", "v")
	$Area2D.set_meta("v", "Dave")
	$Area2D.set_meta("p", self)
	
	enascensor = false
	ignore_colision0 = false
	vaparriba = false
	empezar_a_levitar_terminada_animacion = false

func _process(delta):
	if Engine.editor_hint: return

	var posRedonda = Handle.redondear(position)
	$Sprite.position.x = posRedonda.x - position.x
	$Sprite.position.y = posRedonda.y - position.y
	
	if not toca_suelo:
		if state == Handle.State.Cayendo: _animar_caida()
		elif state == Handle.State.Escalando: _animar_escalera(_get_propio_AABB())

func _controlarA():
	if Input.is_action_just_pressed("A"):
		if abs(motion.x) >= MAX_RUN_SPEED * 0.5:
			_set_state(Handle.State.Corneando)
			Input.action_release("A")
			return true
	return false

func _controlarB():
	if Input.is_action_just_pressed("B"):
		_despegar()
		return true
	return false

func _controlarC():
	if Input.is_action_just_pressed("C"):
		if has_meta("switch"):
			get_meta("switch")._change()
			Input.action_release("C")
			return true
		if has_meta("boton"):
			get_meta("boton")._press()
			Input.action_release("C")
			return true
		if has_meta("npc"):
			get_meta("npc")._iniciar_conversacion(color)
			Input.action_release("C")
			return true
		if toca_suelo and has_meta("teleport"):
			_set_state(Handle.State.Teletransportandose)
			Handle.sound.play_sound("tele_out")
			Input.action_release("C")
			return true
	return false

func _estoy_sobre_suelo():
	if not $RayDown.is_colliding(): return false
	if not $RayDown.get_collider() != null: return false
	var shape = $RayDown.get_collider().get_children().front()
	return shape.has_meta("tc") and shape.get_meta("tc") == "Down"

func _estoy_en_rampa():
	return ($RayLeft.is_colliding() and $RayLeft.get_collider() != null and $RayLeft.get_collider().has_meta("a")) or ($RayRight.is_colliding() and $RayRight.get_collider() != null and $RayRight.get_collider().has_meta("a"))
	
func _rampa_left():
	return direccionX == Handle.Direccion.LEFT and $RayLeft.is_colliding() and $RayLeft.get_collider() != null and $RayLeft.get_collider().has_meta("a")

func _rampa_right():
	return direccionX == Handle.Direccion.RIGHT and $RayRight.is_colliding() and $RayRight.get_collider() != null and $RayRight.get_collider().has_meta("a")



func _acciones_activas(delta):
	match state:
		Handle.State.Esperando:
			_esperarAccion()
		Handle.State.Corriendo:
			if Handle._is_left_or_right_pressed(): 
				_moverse_X(delta, true)
			else: 
				if _rampa_left() or _rampa_right():
					motion.x = 0
				_acciones_pasivas(delta)
			if _controlarA(): return
			if _controlarB(): return
			if _controlarC(): return
			_controlar_escalera()
		Handle.State.Escalando:
			var meta = get_meta("escalera")
			var mitad = meta.position.x + meta.size.x / 2.0
			if abs(mitad - position.x) > 1.0:
				if toca_suelo and $animacion.current_animation != "Walk":
					$animacion.play("Walk")
				var diferencia = mitad - position.x
				position.x += diferencia * 0.4
				if diferencia > 0 and not $Sprite.flip_h:
					$Sprite.flip_h = true
				elif diferencia < 0 and $Sprite.flip_h:
					$Sprite.flip_h = false
				
				if abs(mitad - position.x) <= 1.0:
					position.x = mitad
					$animacion.stop()
					if position.y < meta.position.y + 1:
						position.y = meta.position.y + 1
			else:
				if toca_suelo: toca_suelo = false
				
				if _controlarB(): 
					motion.x = 0.0
					return
				elif Handle._is_left_or_right_pressed() and not Handle._is_up_or_down_pressed():
					if $Sprite.frame != 36 and $Sprite.frame != 37:
						_set_state(Handle.State.Cayendo)
						motion.y = 0.0
						motion.x = 0.0
					else:
						Input.action_press("UP")
						restablecerUP = true
					if not MIRA_DERECHA and $Sprite.flip_h: $Sprite.flip_h = false
					elif MIRA_DERECHA and not $Sprite.flip_h: $Sprite.flip_h = true
				if Input.is_action_pressed("UP"): position.y -= delta * 62.0
				if Input.is_action_pressed("DOWN"): position.y += delta * 62.0
				
				
		Handle.State.Cayendo:
			if Handle._is_left_or_right_pressed(): _moverse_X(delta, true)
			else:
				if direccionX != Handle.Direccion.QUIETO:
					direccionX = Handle.Direccion.QUIETO
				_desacelerar(delta)
			if Input.is_action_pressed("B"):
				if empujones_salto > 0 and Handle.totalTime >= ultimoEmpujon + TIME_ENTRE_EMPUJON:
					empujones_salto-=1
					motion.y -= (FUERZA_SALTO + abs(motion.x) * FUERZA_SALTO_EXTRA_VELOCIDAD / MAX_RUN_SPEED) * 0.3
					ultimoEmpujon = Handle.totalTime
			else: empujones_salto = 0
			if _controlarC(): return
			_controlar_escalera()
		Handle.State.Descansando:
			_esperarAccion()
			_desacelerar(delta)
		Handle.State.Empujando:
			if _controlarB(): return
			if _controlarC(): return
			if sentido_empuja != MIRA_DERECHA :
				_set_state(Handle.State.Descansando)
			elif not ((Input.is_action_pressed("LEFT") and not sentido_empuja) or (Input.is_action_pressed("RIGHT") and sentido_empuja)):
				_set_state(Handle.State.Descansando)
			elif Input.is_action_pressed("RIGHT") and not colision & 8:
				_set_state(Handle.State.Descansando)
			elif Input.is_action_pressed("LEFT") and not colision & 4:
				_set_state(Handle.State.Descansando)
			_controlar_ascensor()
		Handle.State.Corneando:
			if (Input.is_action_pressed("LEFT") and not $Sprite.flip_h) or (Input.is_action_pressed("RIGHT") and $Sprite.flip_h): _moverse_X(delta, false)
			elif $animacion.current_animation_position < 1.32:
				$animacion.seek(1.32)

func _acciones_pasivas(delta):
	match state:
		Handle.State.Corriendo:
			if toca_suelo and state == Handle.State.Corriendo:
				if Handle.totalTime - tiempoCorriendo > 0.5:
					_set_state(Handle.State.Descansando)
				else:
					_set_state(Handle.State.Esperando)
			if direccionX != Handle.Direccion.QUIETO:
				direccionX = Handle.Direccion.QUIETO
		Handle.State.Cayendo:
			if direccionX != Handle.Direccion.QUIETO:
				direccionX = Handle.Direccion.QUIETO
			_desacelerar(delta)
			empujones_salto = 0
		Handle.State.Descansando:
			_desacelerar(delta)
		Handle.State.Empujando:
			_set_state(Handle.State.Descansando)
		Handle.State.Corneando:
			if $animacion.current_animation_position < 1.32:
				$animacion.seek(1.32)

#para acceder desde handle
func _set_direccion_mira(valor):
	if state == Handle.State.Muriendo: return
	MIRA_DERECHA = valor

func _physics_process(delta):
	if Engine.editor_hint: return
	if vaparriba and state != Handle.State.Atontado: 
		motion.y += delta * -Handle.GRAVITY * 0.1
	else: motion.y += delta * Handle.GRAVITY

	
	if Handle.vikingoSelec == self: _acciones_activas(delta)
	else: _acciones_pasivas(delta)
	
	if state == Handle.State.Escalando or state == Handle.State.Chocando or state == Handle.State.Danio:
		return
	
	if not ignore_colision0: colision = 0
	else: ignore_colision0 = false
	
	
	var mueve = motion
	if toca_suelo:
		if _estoy_sobre_suelo():
			mueve.y = 0
		
		if abs(motion.x) >= 1.0: 
			mueve.y = 200.0
			if _rampa_left():
				mueve.x *= 1 + abs($RayLeft.get_collider().get_meta("a")) * 2.0 / 45.0
			elif _rampa_right():
				mueve.x *= 1 + abs($RayRight.get_collider().get_meta("a")) * 2.0 / 45.0
		elif not _estoy_en_rampa():
			mueve.y = 200.0
	move_and_slide(mueve)
	
	if state == Handle.State.Muriendo: return
	
	
	for n in get_slide_count():
		var shape = get_slide_collision(n).collider_shape
		if not shape.has_meta("tc"): continue
		match shape.get_meta("tc"):
			"Down":
				colision = colision | 1
				motion.y = 5.0
				if not toca_suelo:
					toca_suelo = true
					if $Sprite.frame != 23:
						if not vaparriba:
							if Handle._is_left_or_right_pressed(): 
								_set_state(Handle.State.Corriendo)
							else: _set_state(Handle.State.Esperando)
							Handle.sound.play_sound("cae")
						else: 
							toca_suelo = false
							motion.y = -5.0
					else: 
						var miniV = _get_miniVikingo()
						if miniV == null or miniV._vidaSuma(-1):
							_set_state(Handle.State.Atontado)
							Handle.sound.play_sound("caida1")
							motion.x = 0
						else: 
							_salir_estado()
							_matar("CaeAltura")
							Handle.shakeCameraBegin()
							Handle.sound.play_sound("cae_altura")
					
					
				if (colision & 8 != 0 and $Sprite.flip_h) or (colision & 4 != 0 and not $Sprite.flip_h):
					if state == Handle.State.Corriendo:
						_set_state(Handle.State.Empujando)
						sentido_empuja = (colision & 8 != 0)
			"Up":
				colision = colision | 2
				if not vaparriba:
					if motion.y < 0.0:
						motion.y = 0.0
				else:
					motion.y = -motion.y / 2.0
			"Left":
				colision = colision | 4
				if colision & 1 != 0:
					if state == Handle.State.Corriendo and not $Sprite.flip_h:
						_set_state(Handle.State.Empujando)
						sentido_empuja = false
				if state == Handle.State.Cayendo:
					motion.x = 0.0
			"Right":
				colision = colision | 8
				if colision & 1 != 0:
					if state == Handle.State.Corriendo and $Sprite.flip_h:
						_set_state(Handle.State.Empujando)
						sentido_empuja = true
				if state == Handle.State.Cayendo:
					motion.x = 0.0
	
	if colision & 1 == 0 and not _estoy_sobre_suelo():
		if toca_suelo :
			_set_state(Handle.State.Cayendo)
			toca_suelo = false
			motion.y = 1.0
	




func _dejar_de_empujar():
	motion.x = 0.0
	_set_state(Handle.State.Descansando)

func _salir_estado():
	match state:
		Handle.State.Danio:
			position += $Sprite.offset
			$Sprite.offset = Vector2()
			$Area2D.position = Vector2()
			$colision.position = colisionOffset
		Handle.State.Descansando:
			$Sprite.offset = Vector2()
		Handle.State.Cayendo:
			$Area2D.position = Vector2()
			$Sprite.offset = Vector2()
			motion.y = 1.0
		Handle.State.Empujando:
			$Sprite.offset = Vector2()
			motion.x = 0.0
		Handle.State.Corneando:
			$Area2D.position = Vector2()
		Handle.State.Chocando:
			if $Sprite.flip_h: position += Vector2(-20.0, 0.0)
			else: position += Vector2(20.0, 0.0)
			$Sprite.offset = Vector2()
			$colision.position = colisionOffset
			Handle.sound.stop_sound("choca")
		Handle.State.Atontado:
			$colision.position = colisionOffset
			$Area2D.position = Vector2()
			$Sprite.offset = Vector2()
			Handle.sound.stop_sound("estrellitas")
			Handle.sound.stop_sound("caida1")
		Handle.State.Teletransportandose:
			$Sprite.offset = Vector2()
			

func _set_state(newState):
	_salir_estado()
	match newState:
		Handle.State.Esperando:
			$animacion.play("Idle")
			motion.x = 0.0
		Handle.State.Descansando:
			if $Sprite.flip_h: $animacion.play("DescansarR")
			else: $animacion.play("DescansarL")
		Handle.State.Corriendo:
			$animacion.play("Walk")
			tiempoCorriendo = Handle.totalTime
		Handle.State.Cayendo:
			$animacion.stop()
			ultimoEmpujon = Handle.totalTime
		Handle.State.Escalando:
			$animacion.stop()
			$Sprite.offset = Vector2()
		Handle.State.Empujando:
			if $Sprite.flip_h: $animacion.play("EmpujandoR")
			else: $animacion.play("EmpujandoL")
			motion.x = sign(motion.x) * MAX_RUN_SPEED
		Handle.State.Atontado:
			if $Sprite.flip_h: $animacion.play("AtontadoR")
			else: $animacion.play("AtontadoL")
			if state != Handle.State.Chocando:
				Handle.shakeCameraBegin()
			if vaparriba:
				empezar_a_levitar_terminada_animacion = true
		Handle.State.Corneando:
			if $Sprite.flip_h: $animacion.play("CorneadaR")
			else: $animacion.play("CorneadaL")
		Handle.State.Chocando:
			if $Sprite.flip_h: $animacion.play("ChocaR")
			else: $animacion.play("ChocaL")
			motion = Vector2()
			Handle.shakeCameraBegin()
			Handle.sound.play_sound("choca")
		Handle.State.Teletransportandose:
			$animacion.play("TeleportOut")
			motion = Vector2()
	state = newState

func _animar_escalera(AABBpropio):
	var AABBescalera = get_meta("escalera")
	if AABBpropio.position.y + AABBpropio.size.y > AABBescalera.position.y + AABBescalera.size.y:
		if Input.is_action_pressed("DOWN"):
			toca_suelo = true
			motion.y = 5.0
			motion.x = 0.0
			position.y = AABBescalera.position.y + AABBescalera.size.y - 17
			ignore_colision0 = true
			colision = colision | 1
			_set_state(Handle.State.Esperando)
			$Sprite.offset = Vector2()
	if AABBpropio.position.y > AABBescalera.position.y - 8:
		$Sprite.frame = int(position.y / 7) % 4 + 32
		$Sprite.offset = Vector2()
	elif position.y > AABBescalera.position.y - 8:
		if position.y - (AABBescalera.position.y - 8) < 8: 
			$Sprite.frame = 37
			$Sprite.offset.y = (AABBescalera.position.y - 1) - position.y
		else: 
			$Sprite.frame = 36
			$Sprite.offset.y = (AABBescalera.position.y + 8) - position.y
	else:
		if Input.is_action_pressed("UP"):
			if restablecerUP:
				Input.action_release("UP")
				restablecerUP = false
			toca_suelo = true
			motion.y = 5.0
			motion.x = 0.0
			position.y = AABBescalera.position.y - 9
			ignore_colision0 = true
			colision = colision | 1
			_set_state(Handle.State.Esperando)
			$Sprite.offset = Vector2()
	

func _animar_caida():
	if motion.y >= 266.0:
		if $Sprite.frame != 23:
			$Sprite.frame = 23
		return
	if abs(motion.y) < 50.0:
		if $Sprite.frame != 20:
			$Sprite.frame = 20
			if $Sprite.flip_h: 
				$Sprite.offset.x = -6
				$Area2D.position = Vector2(0, -3)
			else: 
				$Sprite.offset.x = 6
				$Area2D.position = Vector2(0, -3)
	elif motion.y < 0:
		if $Sprite.frame != 19:
			$Sprite.frame = 19
			if $Sprite.flip_h: 
				$Sprite.offset.x = 1
				$Area2D.position = Vector2(0, -3)
			else: 
				$Sprite.offset.x = -1
				$Area2D.position = Vector2(0, -3)
			
	else:
		if $Sprite.frame == 20:
			if $Sprite.flip_h: $Area2D.position = Vector2(0, -3)
			else: $Area2D.position = Vector2(0, -3)
			$Sprite.offset = Vector2()
		if $Sprite.frame != 21:
			if ultimoEmpujon + 0.15 < Handle.totalTime:
				$Sprite.frame = 21
				ultimoEmpujon = Handle.totalTime
		elif $Sprite.frame != 22:
			if ultimoEmpujon + 0.15 < Handle.totalTime:
				$Sprite.frame = 22
				ultimoEmpujon = Handle.totalTime
		

func _esperarAccion():
	if Handle._is_left_or_right_pressed(): 
		_direccionarse()
		return
	if _controlarB(): return
	if not _controlar_escalera(): return
	if state == Handle.State.Escalando: return
	
	if _controlarC(): return
	_controlar_ascensor()
	
	
	

func _direccionarse():
	if $Sprite.flip_h != MIRA_DERECHA:
		$Sprite.flip_h = MIRA_DERECHA
	
	if state != Handle.State.Cayendo:
		if state != Handle.State.Corriendo:
			_set_state(Handle.State.Corriendo)
	
	if not MIRA_DERECHA: 
		if direccionX != Handle.Direccion.LEFT:
			if motion.x > MAX_RUN_SPEED * 0.7:
				Handle.sound.play_sound("patina")
			motion.x *= 0.7
			direccionX = Handle.Direccion.LEFT
		
		if _rampa_left():
			motion.x = -MAX_RUN_SPEED * 0.5
	elif MIRA_DERECHA: 
		if direccionX != Handle.Direccion.RIGHT:
			if motion.x < -MAX_RUN_SPEED * 0.7:
				Handle.sound.play_sound("patina")
			motion.x *= 0.7
			direccionX = Handle.Direccion.RIGHT
		
		if _rampa_right():
			motion.x = MAX_RUN_SPEED * 0.5

func _moverse_X(delta, redireccionarse):
	if MIRA_DERECHA:
		if direccionX == Handle.Direccion.RIGHT:
			if motion.x < MAX_RUN_SPEED:
				motion.x += delta * (MAX_RUN_SPEED * 2)
			elif motion.x >= MAX_RUN_SPEED:
				motion.x = MAX_RUN_SPEED
		elif redireccionarse: _direccionarse()
	else:
		if direccionX == Handle.Direccion.LEFT:
			if motion.x > -MAX_RUN_SPEED:
				motion.x -= delta * (MAX_RUN_SPEED * 2)
			elif motion.x <= -MAX_RUN_SPEED:
				motion.x = -MAX_RUN_SPEED
		elif redireccionarse: _direccionarse()

func _despegar():
	if not toca_suelo and state != Handle.State.Escalando: return
	if _rampa_left() or _rampa_right(): position.y -= 5
	_set_state(Handle.State.Cayendo)
	#set_collision_mask_bit(4,0)
	toca_suelo = false
	empujones_salto = MAX_EMPUJONES_SALTO
	motion.y = -(FUERZA_SALTO + abs(motion.x) * FUERZA_SALTO_EXTRA_VELOCIDAD / MAX_RUN_SPEED)
	if colision & 1:
		colision ^= colision & 1

func _desacelerar(delta):
	if abs(motion.x) <= 0.0005: return
	var multiplica = 6
	if not toca_suelo:
		multiplica = 1
	if $Sprite.flip_h:
		motion.x -= delta * (MAX_RUN_SPEED * multiplica)
		if motion.x < 0.0005:
			motion.x = 0.0
	else:
		motion.x += delta * (MAX_RUN_SPEED * multiplica)
		if motion.x > -0.0005:
			motion.x = 0.0

func _on_animation_finished(anim_name):
	if state == Handle.State.Descansando or state == Handle.State.Danio:
		_set_state(Handle.State.Esperando)
	elif state == Handle.State.Atontado:
		if empezar_a_levitar_terminada_animacion:
			state = Handle.State.Cayendo
			_empezar_a_levitar()
			empezar_a_levitar_terminada_animacion = false
		else:
			_set_state(Handle.State.Esperando)
	elif state == Handle.State.Corneando:
		_set_state(Handle.State.Descansando)
	elif state == Handle.State.Chocando:
		_set_state(Handle.State.Atontado)
		Handle.sound.play_sound("estrellitas")
		$animacion.seek(0.2)
	elif state == Handle.State.Muriendo:
		var miniV = _get_miniVikingo()
		miniV.frame = 2
		if Handle.vikingoSelec == self:
			miniV.itemSelecPos = 0 
			if not Handle.cambiar_al_vik_de_la_derecha():
				Handle._salir_del_nivel(Handle.Escena.Mar)
		queue_free()
	elif state == Handle.State.Teletransportandose:
		if anim_name == "TeleportOut":
			position = get_meta("teleport").posicionVa
			$animacion.play("TeleportIn")
			Handle.sound.play_sound("tele_in")
			if $Sprite.flip_h: $Sprite.offset = Vector2(1, 0)
			else: $Sprite.offset = Vector2(-1, 0)
		else:
			_set_state(Handle.State.Esperando)
			
	
	

func _on_area_entered(area):
	if state == Handle.State.Muriendo or not area.has_meta("tc"): return
	match area.get_meta("tc"):
		"ascensor":
			set_meta("ascensor",area.get_meta("p"))
			enascensor = true
		"escalera":
			set_meta("escalera",area.get_meta("aabb"))
		"switch":
			set_meta("switch",area.get_meta("p"))
		"peo":
			set_collision_mask_bit(11,1)
		"boton":
			set_meta("boton",area.get_meta("p"))
		"npc":
			set_meta("npc", area.get_meta("p"))
		"teleport":
			set_meta("teleport",area.get_meta("p"))
		"cerr":
			set_meta("cerr",area.get_meta("p"))
		

func _on_area_exited(area):
	if state == Handle.State.Muriendo or not area.has_meta("tc"): return
	match area.get_meta("tc"):
		"ascensor":
			set_meta("ascensor",null)
			enascensor = false
			if toca_suelo:
				motion.y = 5.0
		"escalera":
			if not (has_meta("escalera") and get_meta("escalera") != area.get_meta("aabb")):
				set_meta("escalera",null)
		"switch":
			set_meta("switch",null)
		"peo":
			set_collision_mask_bit(11,0)
		"boton":
			set_meta("boton",null)
		"npc":
			if not (has_meta("npc") and get_meta("npc") != area.get_meta("p")):
				set_meta("npc",null)
		"teleport":
			set_meta("teleport",null)
		"cerr":
			set_meta("cerr",null)
	
	

func _get_propio_AABB():
	return AABB(Vector3(position.x - 4, position.y - 15, 0), Vector3(16, 31, 0))

func _puede_subir_escalera(AABBescalera):
	if state > 3: return false
	var propioAABB = _get_propio_AABB()
	if propioAABB.position.y < AABBescalera.position.y: return false
	if position.x < AABBescalera.position.x: return false
	if position.x > AABBescalera.position.x + AABBescalera.size.x: return false
	return true

func _puede_bajar_escalera(AABBescalera):
	if state > 3 or not toca_suelo: return false
	var propioAABB = _get_propio_AABB()
	if propioAABB.position.y > AABBescalera.position.y: return false
	if position.x < AABBescalera.position.x: return false
	if position.x > AABBescalera.position.x + AABBescalera.size.x: return false
	return true

#Devuelve true si no están las dos teclas presionadas a la vez
func _controlar_escalera():
	if Handle._is_up_and_down_pressed(): return false
	if Input.is_action_pressed("UP"):
		if has_meta("escalera"):
			if _puede_subir_escalera(get_meta("escalera")):
				_set_state(Handle.State.Escalando)
	if Input.is_action_pressed("DOWN"):
		if has_meta("escalera"):
			if _puede_bajar_escalera(get_meta("escalera")):
				_set_state(Handle.State.Escalando)
	return true

func _controlar_ascensor():
	if Input.is_action_pressed("UP"):
		if has_meta("ascensor"):
			get_meta("ascensor")._subir()
	if Input.is_action_pressed("DOWN"):
		if has_meta("ascensor"):
			get_meta("ascensor")._bajar()

func _empezar_a_levitar():
	if state == Handle.State.Muriendo or antigrav: return
	if state == Handle.State.Atontado:
		empezar_a_levitar_terminada_animacion = true
		return
	vaparriba = true
	toca_suelo = false
	state = Handle.State.Cayendo
	ultimoEmpujon = Handle.totalTime
	$animacion.stop()
	Handle.sound.play_sound("vaparriba")

func _terminar_de_levitar():
	if state == Handle.State.Muriendo: return
	vaparriba = false
	empezar_a_levitar_terminada_animacion = false

func _antigravedad():
	antigrav = true
	_terminar_de_levitar()

func _get_miniVikingo():
	return Handle._get_mini("Dave")

func get_offset():
	if state == Handle.State.Descansando or state == Handle.State.Muriendo or state == Handle.State.Cayendo:
		return Vector2()
	return $Sprite.offset

func _danio(posicionDanio, vidaBaja, animacionMuere):
	if INMORTAL or state == Handle.State.Muriendo or state == Handle.State.Teletransportandose: return
	_salir_estado()
	var miniVikingo = _get_miniVikingo()
	if miniVikingo == null or miniVikingo._vidaSuma(-vidaBaja):
		_activar_danio(posicionDanio)
	else: 
		match animacionMuere:
			1:
				Handle.sound.play_sound("desintegracion")
				_matar("Desintegracion")
			2:
				_kill()

func _activar_danio(posicion):
	if state == Handle.State.Escalando:
		if $Sprite.frame == 17 or $Sprite.frame == 26:
			position.y = get_meta("escalera").position.y - 8
		motion.y = 0
	Handle.sound.play_sound("danio")
	state = Handle.State.Danio
	if posicion.x < position.x:
		$animacion.play("DanioFromL")
	else: $animacion.play("DanioFromR")

func _matar(nombreAnimacion):
	if INMORTAL or state == Handle.State.Muriendo: return
	state = Handle.State.Muriendo
	$animacion.play(nombreAnimacion)
	var miniV = _get_miniVikingo()
	miniV._vidaSuma(-miniV.HP)
	miniV._vaciar_inventario()
	
	var es_kill = (nombreAnimacion == "EsqueletoL" or nombreAnimacion == "EsqueletoR")
	if not es_kill:
		set_physics_process(false)
	set_process(false)
	for i in 20:
		if i < 5 and es_kill: continue
		set_collision_layer_bit(i,0)
		if es_kill: set_collision_mask_bit(i,i < 5)
		else: set_collision_mask_bit(i,0)
		$Area2D.set_collision_layer_bit(i,0)
		$Area2D.set_collision_mask_bit(i,0)

func _kill():
	if $Sprite.flip_h: _matar("EsqueletoR")
	else: _matar("EsqueletoL")
	Handle.sound.play_sound("danio")


func _on_area_cuerno_body_entered(body):
	if state != Handle.State.Corneando: return
	var shape = body.get_children().front()
	if not shape.has_meta("tc"): return
	var meta = shape.get_meta("tc")
	if meta == "Left" or meta == "Right":
		_set_state(Handle.State.Chocando)
	


func _on_area_cuerno_area_entered(area):
	if state != Handle.State.Corneando or not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "barrera" and meta != "e": return
	if meta == "barrera":
		area.get_meta("p")._detonar()
	else:
		area.get_meta("p")._damage(global_position, 1)
		if CHISPA1 != null:
			var chispa = CHISPA1.instance()
			Handle.actualLevel.add_child(chispa)
			if $Sprite.flip_h: chispa._play(position + Vector2(26,-3))
			else: chispa._play(position + Vector2(-26,-3))
		
	_set_state(Handle.State.Chocando)


func _on_Timer2_timeout():
	show()
	$Timer3.stop()

func _on_Timer3_timeout():
	if is_visible_in_tree(): hide()
	else: show()

func _sonido_esqueleto():
	Handle.sound.play_sound("esqueleto")
func _sonido_empuja():
	Handle.sound.play_sound("empuja")

func _empezar_a_titilar():
	$Timer2.start()
	$Timer3.start()

func _dejar_de_titilar():
	$Timer2.stop()
	$Timer2.emit_signal("timeout")













