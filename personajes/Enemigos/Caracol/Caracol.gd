tool
extends KinematicBody2D

export(bool) var MIRA_DERECHA = true setget setmira
export(PackedScene) var escupitajo = null

onready var Handle = get_node("/root/Handle")

var motion = Vector2()
var punteros_ataca = []
var activo = false
var mira_derecha_original
var can_move = true
var can_shoot = true
var can_try = true


func setmira(newVal):
	MIRA_DERECHA = newVal
	if not Engine.editor_hint and not is_inside_tree(): return
	$Sprite.flip_h = MIRA_DERECHA
	if MIRA_DERECHA: 
		$Area_ataca.position.x = 72
		$area_danio.position.x = 3
		$area_caparazon.position.x = -3
	else: 
		$Area_ataca.position.x = -72
		$area_danio.position.x = -3
		$area_caparazon.position.x = 3


func _ready():
	if Engine.editor_hint: return
	get_parent().HP = 2
	self.MIRA_DERECHA = MIRA_DERECHA
	mira_derecha_original = MIRA_DERECHA
	motion = Vector2(-18.0, Handle.GRAVITY)
	set_physics_process(false)
	set_meta("tc","e")
	set_meta("p",self)
	$area_danio.set_meta("tc","e")
	$area_danio.set_meta("p",self)
	$area_caparazon.set_meta("tc","e")
	$area_caparazon.set_meta("p",self)

func _physics_process(delta):
	if Engine.editor_hint: return
	
	if (MIRA_DERECHA and motion.x < 0) or (not MIRA_DERECHA and motion.x > 0):
		motion.x = -motion.x
	
	if not punteros_ataca.empty() and $AnimationPlayer.current_animation != "Ataca":
		if can_shoot or (can_try and get_enemigo_mas_cercano() < 26):
			can_move = false
			$AnimationPlayer.play("Ataca")
			can_shoot = false
	
	var mueve = motion
	if not can_move:
		mueve.x = 0.0
	
	move_and_slide(mueve)
	
	for n in get_slide_count():
		var shape = get_slide_collision(n).collider_shape
		if shape.has_meta("tc") and shape.get_meta("tc") != "Down":
			self.MIRA_DERECHA = not MIRA_DERECHA
			

func _disparar():
	var escup = escupitajo.instance()
	if $Sprite.flip_h: escup._salir(global_position + Vector2(10, 10), true)
	else: escup._salir(global_position + Vector2(-10, 10), false)
	Handle.actualLevel.add_child(escup)
	if get_parent().entes_cercanos_monstruo.find("cam") != -1:
		Handle.sound.play_sound("escupitajo")

func _on_area_danio_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "flecha": return
	var flecha = body.get_meta("p")
	if flecha.efectiva:
		flecha._activar()
		_damage(flecha.global_position)
		flecha.efectiva = false
	else:
		set_collision_layer_bit(10, 1)
		
	

func _on_area_ataca_entered(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "eo": return
	punteros_ataca.push_back(area)
	if punteros_ataca.size() == 1 and can_shoot:
		var vikingo = area.get_meta("p")
		if vikingo.state == Handle.State.Corriendo and vikingo.MIRA_DERECHA != MIRA_DERECHA:
			$Timer.wait_time = 0.85
			$Timer.start()
			can_shoot = false
		else:
			can_move = false
			can_shoot = false
			$AnimationPlayer.play("Ataca")
	

func _on_area_ataca_exited(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "eo": return
	punteros_ataca.erase(area)
	if not punteros_ataca.empty():
		var apunta_bien = false
		for p in punteros_ataca:
			if (MIRA_DERECHA and p.global_position.x > global_position.x) or (not MIRA_DERECHA and p.global_position.x < global_position.x):
				apunta_bien = true
		if not apunta_bien:
			self.MIRA_DERECHA = not MIRA_DERECHA

func _damage(global_pos_danio, danio = 1):
	if get_parent().HP - danio <= 0:
		get_parent()._death()
	else:
		get_parent().HP -= danio
		
		if global_pos_danio.x < global_position.x:
			$AnimationPlayer.get_animation("Danio").track_set_key_value(1,1,Vector2(6,0))
			#if MIRA_DERECHA:
			#	self.MIRA_DERECHA = false
		else:
			$AnimationPlayer.get_animation("Danio").track_set_key_value(1,1,Vector2(-6,0))
			#if not MIRA_DERECHA:
			#	self.MIRA_DERECHA = true
		
		$AnimationPlayer.play("Danio")
		#if not $Timer.is_stopped():
		#	$Timer.start()
		Handle.sound.play_sound("golpe")

func _activar(valor):
	set_physics_process(valor)
	activo = valor
	if activo:
		position = Vector2()
		$AnimationPlayer.play("Walk")
		can_shoot = true
	else:
		self.MIRA_DERECHA = mira_derecha_original
	set_physics_process(valor)

func _girar():
	self.MIRA_DERECHA = not MIRA_DERECHA

func _kill():
	get_parent()._death()


func _on_animation_finished(anim_name):
	if anim_name == "Danio":
		var valor = $AnimationPlayer.get_animation("Danio").track_get_key_value(1,1).x
		if valor == -6:
			if not MIRA_DERECHA:
				self.MIRA_DERECHA = true
		else:
			if MIRA_DERECHA:
				self.MIRA_DERECHA = false
				
	can_try = true
	var distancia = get_enemigo_mas_cercano()
	if distancia > 32:
		$AnimationPlayer.play("Walk")
		can_move = true
		$Timer.wait_time = 0.75
	else:
		$Timer.wait_time = 0.1
	$Timer.start()
	
		


func _on_Timer_timeout():
	can_shoot = true

func get_enemigo_mas_cercano():
	can_try = false
	$Timer2.start()
	var distancia = 100
	for pa in punteros_ataca:
		var dist = Handle.distancia(pa.global_position, global_position)
		if dist < distancia:
			distancia = dist
	return distancia

func _on_Timer2_timeout():
	can_try = true


func _on_caparazon_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "flecha": return
	body.get_meta("p")._destruir()
	
