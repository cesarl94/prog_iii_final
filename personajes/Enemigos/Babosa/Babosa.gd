tool
extends KinematicBody2D

export(bool) var CAN_MOVE = true
export(bool) var MIRA_DERECHA = false setget setmira
export(PackedScene) var Laser = null

onready var Handle = get_node("/root/Handle")

var motion = Vector2()
var punteros_ataca = []
var activo = false
var mira_derecha_original

func setmira(newVal):
	MIRA_DERECHA = newVal
	if not Engine.editor_hint and not is_inside_tree(): return
	$Sprite.flip_h = MIRA_DERECHA
	if MIRA_DERECHA: $Area_ataca.position.x = 12
	else: $Area_ataca.position.x = -12

func _ready():
	if Engine.editor_hint: return
	self.MIRA_DERECHA = MIRA_DERECHA
	mira_derecha_original = MIRA_DERECHA
	motion = Vector2(-49.0, Handle.GRAVITY)
	set_physics_process(false)
	set_meta("tc","e")
	set_meta("p",self)
	$area_danio.set_meta("tc","e")
	$area_danio.set_meta("p",self)
	

func _physics_process(delta):
	if Engine.editor_hint: return
	
	if (MIRA_DERECHA and motion.x < 0) or (not MIRA_DERECHA and motion.x > 0):
		motion.x = -motion.x
	
	var mueve = motion
	if not CAN_MOVE:
		mueve.x = 0.0
	
	move_and_slide(mueve)
	
	for n in get_slide_count():
		var shape = get_slide_collision(n).collider_shape
		if shape.has_meta("tc") and shape.get_meta("tc") != "Down":
			self.MIRA_DERECHA = not MIRA_DERECHA
			

func _disparar():
	var laser = Laser.instance()
	if $Sprite.flip_h: laser._salir(global_position + Vector2(10, -10), true)
	else: laser._salir(global_position + Vector2(-10, -10), false)
	Handle.actualLevel.add_child(laser)
	if get_parent().entes_cercanos_monstruo.find("cam") != -1:
		Handle.sound.play_sound("babosa")

func _on_area_danio_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "flecha": return
	var flecha = body.get_meta("p")
	if flecha.efectiva:
		flecha._activar()
		_damage(Vector2())
		flecha.efectiva = false
	else:
		set_collision_layer_bit(10, 1)
		
	

func _on_area_ataca_entered(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "eo": return
	punteros_ataca.push_back(area)
	if punteros_ataca.size() == 1:
		CAN_MOVE = false
		$AnimationPlayer.play("Ataca")
	

func _on_area_ataca_exited(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "eo": return
	punteros_ataca.erase(area)
	if punteros_ataca.empty():
		$AnimationPlayer.play("Walk")
	else:
		var apunta_bien = false
		for p in punteros_ataca:
			if (MIRA_DERECHA and p.global_position.x > global_position.x) or (not MIRA_DERECHA and p.global_position.x < global_position.x):
				apunta_bien = true
		if not apunta_bien:
			self.MIRA_DERECHA = not MIRA_DERECHA



func _damage(global_pos_danio, danio = 1):
	get_parent()._death()
	Handle.sound.play_sound("golpe")

func _activar(valor):
	set_physics_process(valor)
	activo = valor
	if activo:
		position = Vector2()
		$AnimationPlayer.play("Walk")
	else:
		self.MIRA_DERECHA = mira_derecha_original
	set_physics_process(valor)

func _girar():
	self.MIRA_DERECHA = not MIRA_DERECHA

func _kill():
	get_parent()._death()
















