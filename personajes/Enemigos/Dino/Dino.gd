tool
extends KinematicBody2D

export(bool) var MIRA_DERECHA = false setget setmira
export(PackedScene) var Chispa = null

onready var Handle = get_node("/root/Handle")

var motion = Vector2()
var punteros_ataca = []
var activo = false
var mira_derecha_original
var can_move = true
var can_shoot = true
var chispatscn

class objetivo:
	var name
	var pointer

func setmira(newVal):
	MIRA_DERECHA = newVal
	if not Engine.editor_hint and not is_inside_tree(): return
	$Sprite.flip_h = MIRA_DERECHA
	if MIRA_DERECHA: 
		$area_ataca.position.x = 7
	else: 
		$area_ataca.position.x = -7
		

func _ready():
	if Engine.editor_hint: return
	get_parent().HP = 2
	self.MIRA_DERECHA = MIRA_DERECHA
	mira_derecha_original = MIRA_DERECHA
	motion = Vector2(-49.0, Handle.GRAVITY)
	set_physics_process(false)
	set_meta("tc","e")
	set_meta("p",self)
	$area_danio.set_meta("tc","e")
	$area_danio.set_meta("p",self)
	chispatscn = preload("res://effects/Chispas/Chispa2/Chispa2.tscn")

func _estoy_en_rampa():
	
	return ($RayLeft.is_colliding() and $RayLeft.get_collider().has_meta("a")) or ($RayRight.is_colliding() and $RayRight.get_collider().has_meta("a"))
	
func _rampa_left():
	return not MIRA_DERECHA and $RayLeft.is_colliding() and $RayLeft.get_collider().has_meta("a")

func _rampa_right():
	return MIRA_DERECHA and $RayRight.is_colliding() and $RayRight.get_collider().has_meta("a")

func _physics_process(delta):
	if Engine.editor_hint: return
	
	if (MIRA_DERECHA and motion.x < 0) or (not MIRA_DERECHA and motion.x > 0):
		motion.x = -motion.x
	
	var mueve = motion
	if not can_move:
		mueve.x = 0.0
	
	if abs(mueve.x) >= 1.0: 
		mueve.y = 200.0
		if _rampa_left():
			mueve.x *= 1 + abs($RayLeft.get_collider().get_meta("a")) * 5.0 / 45.0
		elif _rampa_right():
			mueve.x *= 1 + abs($RayRight.get_collider().get_meta("a")) * 5.0 / 45.0
		else:
			mueve.y = Handle.GRAVITY / 3
	elif not _estoy_en_rampa():
		mueve.y = 200.0
	else:
		mueve.y = 0
	
	move_and_slide(mueve)
	
	for n in get_slide_count():
		var shape = get_slide_collision(n).collider_shape
		if shape.has_meta("tc") and shape.get_meta("tc") != "Down":
			self.MIRA_DERECHA = not MIRA_DERECHA
			

func _es_un_escudo_falso(objeto):
	return objeto.name == "eo" and objeto.pointer.escudo
	

func _hay_un_escudo():
	for v in punteros_ataca:
		if v.name == "eo":
			return not v.pointer.escudo
	return false

func _morder():
	var mordio = false
	for v in punteros_ataca:
		if v.name == "eo": 
			continue
		v.pointer._danio(global_position,1,2)
		mordio = true
		var chispa = chispatscn.instance()
		Handle.actualLevel.add_child(chispa)
		chispa._play((global_position + v.pointer.global_position) / 2.0)
	if not _hay_un_escudo() and mordio:
		$Timer.start()



func _on_area_ataca_entered(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "eo": return
	
	var obj = objetivo.new()
	obj.name = meta
	obj.pointer = area.get_meta("p")
	punteros_ataca.push_back(obj)
	
	if can_shoot and not _es_un_escudo_falso(obj):
		$AnimationPlayer.play("Ataca")
		if get_parent().entes_cercanos_monstruo.find("cam") != -1:
			Handle.sound.play_sound("mordisco")
		can_shoot = false
		can_move = false
	

func _on_area_ataca_exited(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "eo": return
	
	for p in punteros_ataca:
		var puntero_area = area.get_meta("p")
		if p.pointer == puntero_area:
			punteros_ataca.erase(p)
			return

func _on_animation_finished(anim_name):
	if anim_name == "Danio":
		var toca_escudo = false
		for v in punteros_ataca:
			if v.name == "eo":
				toca_escudo = true
				break
		if not toca_escudo:
			position += $Sprite.offset
		$Sprite.offset = Vector2()
	
	var hay_un_escudo = _hay_un_escudo()
	
	if not (punteros_ataca.empty() or not hay_un_escudo):
		$AnimationPlayer.play("Ataca")
		if get_parent().entes_cercanos_monstruo.find("cam") != -1:
			Handle.sound.play_sound("mordisco")
	else:
		if $Timer.is_stopped():
			can_shoot = true
			can_move = true
			$AnimationPlayer.play("Walk")
		


func _damage(global_pos_danio, danio = 1):
	if $AnimationPlayer.current_animation == "Ataca":
		Handle.sound.stop_sound("mordisco")
	if get_parent().HP - danio <= 0:
		get_parent()._death()
	else:
		get_parent().HP -= danio
		
		if global_pos_danio.x < global_position.x:
			$AnimationPlayer.get_animation("Danio").track_set_key_value(1,1,Vector2(8,0))
		else:
			$AnimationPlayer.get_animation("Danio").track_set_key_value(1,1,Vector2(-8,0))
		
		$AnimationPlayer.play("Danio")
		Handle.sound.play_sound("golpe")
		can_shoot = false
		can_move = false

func _activar(valor):
	set_physics_process(valor)
	activo = valor
	if activo:
		position = Vector2()
		$AnimationPlayer.play("Walk")
		can_shoot = true
	else:
		self.MIRA_DERECHA = mira_derecha_original
	set_physics_process(valor)

func _girar():
	self.MIRA_DERECHA = not MIRA_DERECHA

func _kill():
	get_parent()._death()

func _on_Timer_timeout():
	can_shoot = true
	can_move = true
	$AnimationPlayer.play("Walk")


func _on_danio_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "flecha": return
	var flecha = body.get_meta("p")
	if flecha.efectiva:
		flecha._activar()
		_damage(flecha.global_position)
		flecha.efectiva = false
	else:
		set_collision_layer_bit(10, 1)
