tool
extends KinematicBody2D

export(bool) var MIRA_DERECHA = false setget setmira
export(PackedScene) var Laser = null

onready var Handle = get_node("/root/Handle")

var motion = Vector2()
var punteros_ataca = []
var activo = false
var mira_derecha_original
var can_move = true
var can_shoot = true
var can_try = true


func setmira(newVal):
	MIRA_DERECHA = newVal
	if not Engine.editor_hint and not is_inside_tree(): return
	$Sprite.flip_h = MIRA_DERECHA
	if MIRA_DERECHA: $Area_ataca.position.x = 72
	else: $Area_ataca.position.x = -72

func _ready():
	if Engine.editor_hint: return
	self.MIRA_DERECHA = MIRA_DERECHA
	mira_derecha_original = MIRA_DERECHA
	motion = Vector2(-49.0, Handle.GRAVITY)
	set_physics_process(false)
	set_meta("tc","e")
	set_meta("p",self)
	$area_danio.set_meta("tc","e")
	$area_danio.set_meta("p",self)
	

func _physics_process(delta):
	if Engine.editor_hint: return
	
	if (MIRA_DERECHA and motion.x < 0) or (not MIRA_DERECHA and motion.x > 0):
		motion.x = -motion.x
	
	if not punteros_ataca.empty() and $AnimationPlayer.current_animation != "Ataca":
		if can_shoot or (can_try and get_enemigo_mas_cercano() < 36):
			can_move = false
			$AnimationPlayer.play("Ataca")
			can_shoot = false
	
	var mueve = motion
	if not can_move:
		mueve.x = 0.0
	
	move_and_slide(mueve)
	
	for n in get_slide_count():
		var shape = get_slide_collision(n).collider_shape
		if shape.has_meta("tc") and shape.get_meta("tc") != "Down":
			self.MIRA_DERECHA = not MIRA_DERECHA
			

func _disparar():
	var laser = Laser.instance()
	if $Sprite.flip_h: laser._salir(global_position + Vector2(10, 0), true)
	else: laser._salir(global_position + Vector2(-10, 0), false)
	Handle.actualLevel.add_child(laser)
	if get_parent().entes_cercanos_monstruo.find("cam") != -1:
		Handle.sound.play_sound("canion_laser")

func _on_area_danio_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "flecha": return
	var flecha = body.get_meta("p")
	if flecha.efectiva:
		flecha._activar()
		_damage(Vector2())
		flecha.efectiva = false
	else:
		set_collision_layer_bit(10, 1)
		
	

func _on_area_ataca_entered(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "eo": return
	punteros_ataca.push_back(area)
	if punteros_ataca.size() == 1 and can_shoot:
		var vikingo = area.get_meta("p")
		if vikingo.state == Handle.State.Corriendo and vikingo.MIRA_DERECHA != MIRA_DERECHA:
			$Timer.wait_time = 0.85
			$Timer.one_shot = true
			$Timer.start()
			can_shoot = false
		else:
			can_move = false
			can_shoot = false
			$AnimationPlayer.play("Ataca")
	

func _on_area_ataca_exited(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "eo": return
	punteros_ataca.erase(area)
	if not punteros_ataca.empty():
		var apunta_bien = false
		for p in punteros_ataca:
			if (MIRA_DERECHA and p.global_position.x > global_position.x) or (not MIRA_DERECHA and p.global_position.x < global_position.x):
				apunta_bien = true
		if not apunta_bien:
			self.MIRA_DERECHA = not MIRA_DERECHA



func _damage(global_pos_danio, danio = 1):
	get_parent()._death()
	Handle.sound.play_sound("golpe")

func _activar(valor):
	set_physics_process(valor)
	activo = valor
	if activo:
		position = Vector2()
		$AnimationPlayer.play("Walk")
		can_shoot = true
	else:
		self.MIRA_DERECHA = mira_derecha_original
	set_physics_process(valor)

func _girar():
	self.MIRA_DERECHA = not MIRA_DERECHA

func _kill():
	get_parent()._death()


func _on_animation_finished(anim_name):
	
	can_try = true
	var distancia = get_enemigo_mas_cercano()
	if distancia > 48:
		$AnimationPlayer.play("Walk")
		can_move = true
		$Timer.one_shot = true
		$Timer.wait_time = 0.75
	else:
		$Timer.one_shot = false
		$Timer.wait_time = 0.1
	$Timer.start()


func _on_Timer_timeout():
	can_shoot = true

func get_enemigo_mas_cercano():
	can_try = false
	$Timer2.start()
	var distancia = 100
	for pa in punteros_ataca:
		var dist = Handle.distancia(pa.global_position, global_position)
		if dist < distancia:
			distancia = dist
	return distancia

func _on_Timer2_timeout():
	can_try = true
