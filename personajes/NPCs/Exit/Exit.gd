extends Area2D

export(bool) var va_a_portal = false
var vikingos = []

func _iniciar_conversacion(colorVikingo):
	if not has_node("Conversacion"): return
	var conversacion = get_node("Conversacion")
	conversacion.z_index = -z_index
	conversacion._iniciar_conversacion(colorVikingo)

func _terminada_conversacion():
	if va_a_portal: Handle._salir_del_nivel(Handle.Escena.Portal)
	else: Handle._salir_del_nivel(Handle.Escena.Nivel)
	

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	vikingos.push_back(area.get_meta("p"))
	if vikingos.size() == 3:
		_iniciar_conversacion(Color())
	elif vikingos.size() == Handle.vikingos.get_children().size():
		Handle._salir_del_nivel(Handle.Escena.Mar)

func _on_area_exited(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	vikingos.erase(area.get_meta("p"))
