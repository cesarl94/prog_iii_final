tool
extends Sprite

export(bool) var rubia = true setget setcolor
export(bool) var mira_derecha = false setget setmira

func setcolor(newVal):
	rubia = newVal
	material.set_shader_param("rubia",rubia)
	
func setmira(newVal):
	mira_derecha = newVal
	flip_h = mira_derecha
	

func _ready():
	material.set_shader_param("rubia",rubia)
	flip_h = mira_derecha

func _on_Timer_timeout():
	if frame == 0:
		frame = 1
	else: frame = 0
