tool
extends Area2D

export(bool)var MIRA_DERECHA = false setget mira

onready var Handle = get_node("/root/Handle")

func mira(newVal):
	MIRA_DERECHA = newVal
	if not Engine.editor_hint and not is_inside_tree(): return
	$Sprite.flip_h = MIRA_DERECHA

func _ready():
	if Engine.editor_hint: return
	self.MIRA_DERECHA = MIRA_DERECHA
	set_meta("tc","npc")
	set_meta("p",self)

func _iniciar_conversacion(colorVikingo):
	if not has_node("Conversacion"): return
	var conversacion = get_node("Conversacion")
	conversacion.z_index = -z_index
	conversacion._iniciar_conversacion(colorVikingo)

func _terminada_conversacion():
	pass