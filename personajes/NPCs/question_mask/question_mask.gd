extends Area2D

func _ready():
	set_meta("tc","npc")
	set_meta("p",self)

func _iniciar_conversacion(colorVikingo):
	if not has_node("Conversacion"): return
	var conversacion = get_node("Conversacion")
	conversacion.z_index = -z_index
	conversacion._iniciar_conversacion(colorVikingo)

func _terminada_conversacion():
	pass