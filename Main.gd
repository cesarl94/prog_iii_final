extends Control



var nivel = 1
var android = true



func _ready():
	Handle.main = self
	
	if android:
		var screen_size = OS.get_screen_size()
		screen_size = Vector2(1280, 720)
		var game_size = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height"))
		var escala = screen_size / game_size
		if escala.y < escala.x:
			rect_position.x = ((screen_size.x / escala.y) - game_size.x) / 2.0
		else: get_tree().quit()
	else:
		var control = $Control
		remove_child(control)
		control.queue_free()
	
	
	#$Audio.mutear = true
	if false: #turnar verdadero o falso si queremos eliminar los datos de partida
		var dir = Directory.new()
		dir.remove("user://play.data")
	
	cargar_menu()
	#_comenzar_en(nivel)
	

func cargar_escena():
	if nivel == 4 and Handle.escena_sigue == Handle.Escena.Nivel:
		Handle.escena_sigue = Handle.Escena.Intro
		nivel = 0
	
	
	if Handle.escena_sigue == Handle.Escena.Nivel:
		$VistaJuego.reiniciadas = 0
		nivel += 1
		$Espera._activar($VistaJuego, nivel)
	else:
		if Handle.escena_sigue == Handle.Escena.Portal:
			modulate.a = 1
			Handle._limpiar_level_actual()
			Handle.sound.stop_music()
			$Audio.agregar_volume(1)
			Handle.camara.current = false
			$VistaJuego.hide()
			
			var portal = preload("res://escenas/Portal/Portal.tscn").instance()
			add_child(portal)
			Handle.escena_sigue = Handle.Escena.Nivel
			portal.connect("termina",self,"cargar_escena")
		elif Handle.escena_sigue == Handle.Escena.Mar:
			
			var vikingos = Handle.actualLevel.get_node("Vikingos")
			var dave = vikingos.has_node("Dave")
			var skyler = vikingos.has_node("Skyler")
			var eddy = vikingos.has_node("Eddy")
			
			modulate.a = 1
			Handle._limpiar_level_actual()
			Handle.sound.stop_music()
			$Audio.agregar_volume(1)
			Handle.camara.current = false
			$VistaJuego.hide()
			
			var mar = preload("res://escenas/Mar/Mar.tscn").instance()
			add_child(mar)
			Handle.escena_sigue = Handle.Escena.Nivel
			mar.connect("reiniciar",$VistaJuego,"_reiniciar_level")
			mar.connect("to_menu",self,"cargar_menu")
			mar._comenzar(dave,skyler,eddy)
			
		elif Handle.escena_sigue == Handle.Escena.Intro:
			modulate.a = 1
			Handle._limpiar_level_actual()
			Handle.sound.stop_music()
			$Audio.agregar_volume(1)
			Handle.camara.current = false
			$VistaJuego.hide()
			
			cargar_menu()
			Handle.escena_sigue = Handle.Escena.Nivel
		
		
	
	

func _comenzar_en(nivel_comienza):
	if has_node("Intro"):
		var introduccion = get_node("Intro")
		remove_child(introduccion)
		introduccion.queue_free()
	nivel = nivel_comienza
	$Espera._activar($VistaJuego, nivel)
	


func cargar_menu():
	var introduccion
	if android:
		introduccion = preload("res://escenas/Intro/Intro_android.tscn").instance()
		$Control.activado = false
	else: introduccion = preload("res://escenas/Intro/Intro.tscn").instance()
	introduccion.name = "Intro"
	add_child(introduccion)
	introduccion.connect("go_to_lv",self,"_comenzar_en")

var pasar_de_lv = true

func fade_out(iniciar_level_al_terminar = true):
	var is_playing = $Filtros.is_playing() and $Filtros.current_animation == "FadeIn"
	var pos = $Filtros.current_animation_position
	pasar_de_lv = iniciar_level_al_terminar
	$Filtros.play("FadeOut")
	$Audio.set_process(true)
	if is_playing:
		$Filtros.seek(1.0 - pos)
		modulate.a = pos
	
func fade_in():
	$Filtros.play("FadeIn")
	var agregar_volumen = Handle.sound.is_music_playing()
	if not agregar_volumen:
		Handle.sound.set_volume(1)
	$Audio.set_process(agregar_volumen)

func _on_Filtros_finished(anim_name):
	if anim_name == "FadeOut" and pasar_de_lv:
		cargar_escena()
	$Audio.set_process(false)
