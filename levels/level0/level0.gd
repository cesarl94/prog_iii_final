extends Node2D

export(int) var TILES_X = 144
export(int) var TILES_Y = 24
export(String) var PASSWORD = ""
var PAUSAR_AL_COMENZAR = false
var noche = false
var defasaje = 0

func _ready():
	if Engine.editor_hint: return
	for d in get_node("Collisions").get_children():
		for b in d.get_children():
			b.get_children().front().set_meta("tc", d.get_name()) #tc: tipo colisión, se almacenan como "Up", "Down", "Left" y "Rigth"
			b.set_collision_mask_bit(0,0)
			b.set_collision_layer_bit(Handle._get_mask_from_dir(d.get_name()),1)
	for e in get_node("Escaleras").get_children():
		if not e is Area2D: continue
		e.set_meta("tc", "escalera")
		var ab = AABB(Vector3(1000000, 1000000, 0), Vector3(0, 0, 0))
		for v in e.get_children().front().polygon:
			if v.x < ab.position.x: ab.position.x = v.x
			if v.y < ab.position.y: ab.position.y = v.y
			if v.x > ab.size.x: ab.size.x = v.x
			if v.y > ab.size.y: ab.size.y = v.y
		ab.size.x = ab.size.x - ab.position.x
		ab.size.y = ab.size.y - ab.position.y
		e.set_meta("aabb",ab)
	
	for r in get_node("Rampas").get_children():
		r.get_children().front().set_meta("tc","Down")
		r.set_collision_layer_bit(1,1)
		
		var p1 = r.get_children().front().polygon[0]
		var p2 = r.get_children().front().polygon[1]
		
		var angulo = atan2(p2.y - p1.y, p2.x - p1.x) * 180.0 / PI
		if abs(angulo) > 90.0:
			angulo = 180.0 + angulo
		r.set_meta("a",angulo)
	
	Handle.intercambiar_info_level($Vikingos, self)
	Handle.sound.play_music("village")
	
	set_process(false)
	


var LEFT = false
var RIGHT = false
var A = false
var B = false
var UP = false

	
func _anim_press(tecla, valor):
	if tecla == "LEFT":
		LEFT = valor
	elif tecla == "RIGHT":
		RIGHT = valor
	elif tecla == "A":
		
		A = valor
	elif tecla == "B":
		B = valor
	elif tecla == "UP":
		UP = valor
	else:
		return
	if valor: Input.action_press(tecla)
	else: Input.action_release(tecla)
	

func _activar_globo(numero, sonido):
	for g in $Textos.get_children():
		g.hide()
	if numero != 0:
		$Textos.get_node("GloboText"+String(numero)).show()
	if sonido:
		Handle.sound.play_sound("globo")

func _input(event):
	if event is InputEventKey:
		if event.scancode == KEY_Q:
			if event.pressed != A:
				if event.pressed: Input.action_release("A")
				else: Input.action_press("A")
		if event.scancode == KEY_W:
			if event.pressed != B:
				if event.pressed: Input.action_release("B")
				else: Input.action_press("B")
		if event.scancode == KEY_RIGHT:
			if event.pressed != RIGHT:
				if event.pressed: Input.action_release("RIGHT")
				else: Input.action_press("RIGHT")
		if event.scancode == KEY_LEFT:
			if event.pressed != LEFT:
				if event.pressed: Input.action_release("LEFT")
				else: Input.action_press("LEFT")
		if event.scancode == KEY_UP:
			if event.pressed != UP:
				if event.pressed: Input.action_release("UP")
				else: Input.action_press("UP")
		if event.scancode == KEY_DOWN:
			if event.pressed: Input.action_release("DOWN")
		if event.scancode == KEY_SPACE:
			if event.pressed: Input.action_release("Y")
		if event.scancode == KEY_1:
			if event.pressed: Input.action_release("UNO")
			if event.pressed: Input.action_release("NUM")
		if event.scancode == KEY_2:
			if event.pressed: Input.action_release("DOS")
			if event.pressed: Input.action_release("NUM")
		if event.scancode == KEY_3:
			if event.pressed: Input.action_release("TRES")
			if event.pressed: Input.action_release("NUM")
		if event.scancode == KEY_ENTER:
			activar_fase3()

func ola():
	Handle.sound.play_sound("olas")

func _selec_vikingo(numero):
	var vikingoSelecAntes = Handle.vikingoSelec
	var vikingoNuevo = Handle._get_vikingo_selec(numero)
	if vikingoNuevo != null:
		Handle.vikingoSN = numero
		Handle.vikingoSelec = vikingoNuevo
		Handle.updateVikingo(vikingoSelecAntes)

func _activar_musculos():
	$Vikingos/Skyler/animacion.play("Musculos")

func _acomodar_caracol():
	$Enemigos/Enemigo/Caracol.position.x += 48
	
	
func _activar_fase1():
	#Handle.filtro.position = Handle.camara.get_camera_screen_center() - Vector2(160,112)
	Handle.main.fade_out(false)
	

export(float) var altura_nave

func _activar_fase2():
	#modulate.a = 0
	$Timer.start()
	

func activar_fase3():
	#Handle.filtro.position = Handle.camara.get_camera_screen_center() - Vector2(160,112)
	Handle.main.fade_out()
	Handle.shakeCameraEnd()
	Handle.escena_sigue = Handle.Escena.Nivel


func shake():
	Handle.camara.smoothing_enabled = true
	Handle.shakeCameraBegin(32.7, 8)
	Handle.sound.play_music("nave")

func _estrellitas(valor):
	if valor: $Noche/Nave/Estrellitas.show()
	else: $Noche/Nave/Estrellitas.hide()

func _apagar_z(num):
	$Noche.get_node("ZZZ"+String(num)).get_node("Timer").stop()

func hide_viking(num):
	match num:
		1: $Noche/Dave.hide()
		2: $Noche/Skyler.hide()
		3: $Noche/Eddy.hide()

func _process(delta):
	if noche:
		$Noche/Background.position.x = Vector2(Handle.camara.get_camera_screen_center() - Vector2(160, 112)).x * 0.4
		if not (Handle.camara.get_camera_screen_center().x > 160.1 and Handle.camara.get_camera_screen_center().x < 359.9):
			$Noche/Nave.position.x = $PersigueCamara.position.x
		else:
			$Noche/Nave.position.x = Handle.camara.get_camera_screen_center().x
		$Noche/Nave.position.y = Handle.camara.get_camera_screen_center().y + altura_nave
	else:
		match defasaje:
			1:
				if $Vikingos/Dave.position.x > 740:
					$Vikingos/Dave.position.x = 740
			2:
				if $Vikingos/Dave.toca_suelo:
					_anim_press("A",true)
					controlar_defasaje(0)
			3:
				if $Vikingos/Dave.position.x > 1044:
					$Vikingos/Dave.position.x = 1044
				if $Vikingos/Dave.state == Handle.State.Esperando:
					controlar_defasaje(0)
					_selec_vikingo(2)
					$AnimationPlayer.play("Part6")
			4:
				if $Vikingos/Eddy.toca_suelo:
					_anim_press("RIGHT",false)
					controlar_defasaje(0)
					$AnimationPlayer.play("Part8")
			5:
				if $Vikingos/Skyler.position.x > 1468:
					$Vikingos/Skyler.position.x = 1468
					_anim_press("RIGHT",false)
					$AnimationPlayer.play("Part10")
					controlar_defasaje(0)
			6:
				if $Vikingos/Eddy.position.x > 1392:
					$Vikingos/Eddy.position.x = 1392
					_anim_press("A",true)
					_anim_press("RIGHT",false)
					$AnimationPlayer.play("Part11")
					controlar_defasaje(0)
			7:
				if $Vikingos/Dave.position.x > 1376 and Input.is_action_pressed("RIGHT"):
					_anim_press("RIGHT",false)
					_anim_press("B",true)
					controlar_defasaje(8)
					#$AnimationPlayer.play("Part12")
			8:
				if $Vikingos/Dave.position.x > 1392:
					$Vikingos/Dave.position.x = 1392
					$Vikingos/Dave.motion.x = 0.0
					controlar_defasaje(0)
					poner_suelta("B",false,Vector2(1396, 260), false, "Part13")
			9:
				if $Vikingos/Dave.toca_suelo:
					_anim_press("B",true)
					controlar_defasaje(0)
					$AnimationPlayer.play("Part14")
			10:
				if $Vikingos/Dave.position.x > 1436:
					$Vikingos/Dave.position.x = 1436
					$Vikingos/Dave.motion.x = 0.0
					_anim_press("RIGHT",false)
					$AnimationPlayer.play("Part17")
					controlar_defasaje(0)
			11:
				if $Vikingos/Dave.position.x > 1884:
					$Vikingos/Dave.position.x = 1884
					$Vikingos/Dave.motion.x = 0
				if $Vikingos/Dave.state == Handle.State.Esperando:
					$AnimationPlayer.play("Part21")
					controlar_defasaje(0)
				


func _on_Timer_timeout():
	#modulate.a = 1
	Handle.sound.stop_music()
	for g in $Textos.get_children():
		g.hide()
	get_node("Capa de Patrones 1").material.set_shader_param("noche", true)
	$Familia.hide()
	$ParallaxBackground.hide()
	$Vikingos.hide()
	Handle.vikingoSelec = $PersigueCamara
	Handle.camara.position = $PersigueCamara.position
	Handle.camara.smoothing_enabled = false
	#Handle.filtro.position = Vector2()
	Handle.main.fade_in()
	$Noche.show()
	$AnimationPlayer.play("Noche")
	set_process(true)
	noche = true
	$Noche/ZZZ1/Timer.start()
	$Noche/ZZZ2/Timer.start()
	$Noche/ZZZ3/Timer.start()
	Handle.camara.limit_right = 520
	#$AnimationPlayer.seek(30)

func quitar_suelta():
	$Suelta.position = Vector2()

func poner_suelta(tecla,entra, posicion, apreta = false, anim_sig = ""):
	$Suelta.tecla = tecla
	$Suelta.entra = entra
	$Suelta.position = posicion
	$Suelta.mueve = false
	$Suelta.anim_sigue = anim_sig
	$Suelta.apreta = apreta
	
	
func poner_siguiente_suelta(posicion, otraTecla, apreta = false, anim_sig = "", otra_entra = true):
	$Suelta.mueve = true
	$Suelta.otraPos = posicion
	$Suelta.otraTecla = otraTecla
	$Suelta.otra_anim_sigue = anim_sig
	$Suelta.otraApreta = apreta
	$Suelta.otra_entra = otra_entra

func controlar_defasaje(numero):
	defasaje = numero
	set_process(numero != 0)
	



