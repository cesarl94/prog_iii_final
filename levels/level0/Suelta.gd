extends Area2D

export(String,"RIGHT","UP","A","B") var tecla = "RIGHT"
export(bool)var entra = true
export(bool)var apreta = false
var mueve = false
var otraPos
var otraTecla
var anim_sigue = ""
var otraApreta
var otra_anim_sigue
var otra_entra

func _ready():
	set_process(false)
	set_physics_process(false)

func _on_Suelta_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v" or not entra: return
	if apreta: Input.action_press(tecla)
	else: Input.action_release(tecla)
	if mueve:
		set_process(true)
		mueve = false
		position = Vector2()
		
	if anim_sigue != "":
		get_parent().get_node("AnimationPlayer").play(anim_sigue)

func _process(delta):
	position = otraPos
	tecla = otraTecla
	apreta = otraApreta
	anim_sigue = otra_anim_sigue
	if entra != otra_entra:
		set_physics_process(true)
	set_process(false)

func _physics_process(delta):
	entra = not entra
	set_physics_process(false)

func _on_Suelta_area_exited(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v" or entra: return
	if apreta: Input.action_press(tecla)
	else: Input.action_release(tecla)
	if mueve:
		set_process(true)
		mueve = false
		position = Vector2()
	if anim_sigue != "":
		get_parent().get_node("AnimationPlayer").play(anim_sigue)

