extends Node2D

var usado1 = false
var usado2 = false

func _terminada_conversacion():
	pass

func _iniciar():
	var node = get_node("Inicio")
	node._iniciar_conversacion(Color())

func _on_preg_area_entered(area):
	if usado1 or not area.has_meta("tc") or area.get_meta("tc") != "v": return
	usado1 = true
	get_node("preg")._iniciar_conversacion(area.get_meta("p").color)

func _on_exit_area_entered(area):
	if usado2 or not area.has_meta("tc") or area.get_meta("tc") != "v": return
	usado2 = true
	get_node("ConversacionExit")._iniciar_conversacion(area.get_meta("p").color)
