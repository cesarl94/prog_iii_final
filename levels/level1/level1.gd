extends Node2D

export(int) var TILES_X = 30
export(int) var TILES_Y = 30
export(String) var PASSWORD = "STRT"
var PAUSAR_AL_COMENZAR = false

func _ready():
	for d in get_node("Collisions").get_children():
		for b in d.get_children():
			b.get_children().front().set_meta("tc", d.get_name()) #tc: tipo colisión, se almacenan como "Up", "Down", "Left" y "Rigth"
			b.set_collision_mask_bit(0,0)
			b.set_collision_layer_bit(0,0)
			b.set_collision_layer_bit(Handle._get_mask_from_dir(d.get_name()),1)
	for e in get_node("Escaleras").get_children():
		if not e is Area2D: continue
		e.set_meta("tc", "escalera")
		var ab = AABB(Vector3(1000000, 1000000, 0), Vector3(0, 0, 0))
		for v in e.get_children().front().polygon:
			if v.x < ab.position.x: ab.position.x = v.x
			if v.y < ab.position.y: ab.position.y = v.y
			if v.x > ab.size.x: ab.size.x = v.x
			if v.y > ab.size.y: ab.size.y = v.y
		ab.size.x = ab.size.x - ab.position.x
		ab.size.y = ab.size.y - ab.position.y
		e.set_meta("aabb",ab)
	Handle.intercambiar_info_level($Vikingos, self)
	Handle.sound.play_music("space")
	
	if Handle.main.android:
		var globo = $Misiones/Inicio/GloboText3
		$Misiones/Inicio.remove_child(globo)
		globo.queue_free()
		
		$Misiones/preg/Conversacion/GloboText._change_text("puedes obtener pistas-si te paras al-lado de estos botones-y presionas \'c\'.")
		$Misiones/ConversacionExit/GloboText2._change_text("para cambiar de-personaje, presiona-sobre su imagen-en el tablero de-arriba.")
	

func _process(delta):
	if Engine.editor_hint: return
	$Misiones._iniciar()
	set_process(false)
	
















func _on_inicio_area_entered(area):
	pass # replace with function body
