extends Node2D


var activo = true
var Vaparriba
var vikingos = []

func _parent_button_press():
	if not activo: return
	activo = false
	for v in vikingos:
		v._terminar_de_levitar()
	Vaparriba._stop()

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	var vikingo = area.get_meta("p")
	vikingos.push_back(vikingo)
	if Vaparriba._get_state(): vikingo._empezar_a_levitar()

func _on_area_exited(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	var vikingo = area.get_meta("p")
	vikingos.erase(vikingo)
	vikingo._terminar_de_levitar()

