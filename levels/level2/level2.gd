extends Node2D

export(int) var TILES_X = 64
export(int) var TILES_Y = 51
export(String) var PASSWORD = "GR8T"
var PAUSAR_AL_COMENZAR = false

func _ready():
	if Engine.editor_hint: return
	for d in get_node("Collisions").get_children():
		for b in d.get_children():
			b.get_children().front().set_meta("tc", d.get_name()) #tc: tipo colisión, se almacenan como "Up", "Down", "Left" y "Rigth"
			b.set_collision_mask_bit(0,0)
			b.set_collision_layer_bit(0,0)
			b.set_collision_layer_bit(Handle._get_mask_from_dir(d.get_name()),1)
	for e in get_node("Escaleras").get_children():
		if not e is Area2D: continue
		e.set_meta("tc", "escalera")
		var ab = AABB(Vector3(1000000, 1000000, 0), Vector3(0, 0, 0))
		for v in e.get_children().front().polygon:
			if v.x < ab.position.x: ab.position.x = v.x
			if v.y < ab.position.y: ab.position.y = v.y
			if v.x > ab.size.x: ab.size.x = v.x
			if v.y > ab.size.y: ab.size.y = v.y
		ab.size.x = ab.size.x - ab.position.x
		ab.size.y = ab.size.y - ab.position.y
		e.set_meta("aabb",ab)
	$Antigravedad.get_children().back().set_meta("m",$Vaparriba)
	$Antigravedad.get_children().back()._comenzar()
	
	Handle.intercambiar_info_level($Vikingos, self)
	Handle.sound.play_music("space")
	
	if Handle.main.android:
		var preg = $Misiones/preg
		$Misiones.remove_child(preg)
		preg.queue_free()
		preg = $Misiones/preg7
		$Misiones.remove_child(preg)
		preg.queue_free()
		
		$Misiones/preg2.position.x -= 16
		$Misiones/preg2/Conversacion/GloboText._change_text("para usar un item-presione sobre él-en el inventario.")
		$Misiones/preg3/Conversacion/GloboText._change_text("use la tecla \'c\' para presionar-botones, usar interruptores y-hablar con otros personajes.")
		$Misiones/preg5/Conversacion/GloboText._change_text("Skyler, el feroz, es un semental.-Él puede usar sus flechas para-alternar los interruptores.-así como para derrotar a los-monstruos. para disparar una-flecha, presione \'a\'.")
		$Misiones/preg6/Conversacion/GloboText._change_text("para traspasar un artículo,-presione sobre él y arrástrelo-hasta el vikingo que desee.-Para esto, ambos personajes-deben estar cerca.")
		
		

func _process(delta):
	if Engine.editor_hint: return
	if PAUSAR_AL_COMENZAR:
		
		PAUSAR_AL_COMENZAR = false
		get_tree().paused = true
	set_process(false)


func _apagarLuces():
	Handle.filtro._activar_filtro_rojo()
	$Misiones/Cuidado.queue_free()

var vikingos = []

func _cambiaAntigravedad():
	for v in vikingos:
		if $Vaparriba._get_state(): v._empezar_a_levitar()
		else: v._terminar_de_levitar()

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	var vikingo = area.get_meta("p")
	vikingos.push_back(vikingo)
	if $Vaparriba._get_state(): vikingo._empezar_a_levitar()

func _on_area_exited(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	var vikingo = area.get_meta("p")
	vikingos.erase(vikingo)
	vikingo._terminar_de_levitar()