extends AnimationPlayer

export(Vector3) var sumaColor = Vector3()
var mate


func _ready():
	set_process(false)
	Handle.filtro = self

func _process(delta):
	mate.set_shader_param("colorSuma",sumaColor)

func _activar_filtro_blanco():
	mate.set_shader_param("colorSuma",Vector3())
	set_process(true)
	play("FiltroBlanco")

func _activar_filtro_rojo():
	mate.set_shader_param("colorSuma",Vector3())
	set_process(true)
	play("FiltroRojo")


func _on_animation_finished(anim_name):
	mate.set_shader_param("colorSuma",Vector3())
	set_process(false)

func _activar_alarma():
	if Handle.sound.mutear: return
	$AudioStreamPlayer.volume_db = 0
	$AudioStreamPlayer.play()

func _apagar_alarma():
	$AudioStreamPlayer.stop()

