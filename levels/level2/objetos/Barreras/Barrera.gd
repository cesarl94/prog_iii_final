extends Area2D

var detonado

func _ready():
	set_meta("tc","barrera")
	set_meta("p",self)
	detonado = false

func _detonar():
	if not detonado:
		detonado = true
		for i in 20:
			set_collision_layer_bit(i,0)
			set_collision_mask_bit(i,0)
	
	var piezas = get_node("Piezas").get_children()
	if piezas.size() == 0:
		queue_free()
		return

	var bloque = piezas.front()
	$Humito.position = bloque.position + $Piezas.position
	$Humito._comenzar()
	Handle.sound.play_sound("barrera")

	bloque.queue_free()
	$Timer.start()

func _on_Timer_timeout():
	_detonar()

func _kill():
	_detonar()