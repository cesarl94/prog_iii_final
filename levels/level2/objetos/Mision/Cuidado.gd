extends Area2D

var vikingos = []

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	var vikingo = area.get_meta("p")
	
	for v in vikingos:
		if v == vikingo:
			return
	vikingos.push_back(vikingo)
	vikingo._set_state(Handle.State.Esperando)
	vikingo.motion = Vector2()
	
	if not has_node("Conversacion"): return
	get_node("Conversacion")._iniciar_conversacion(vikingo.color)

func _terminada_conversacion():
	if vikingos.size() == 3:
		queue_free()