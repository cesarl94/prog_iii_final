extends Node2D

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	var vikingo = area.get_meta("p")
	
	if not has_node("Conversacion"): return
	get_node("Conversacion")._iniciar_conversacion(vikingo.color)
	

func _terminada_conversacion():
	get_tree().paused = false
	var aftiek = $Aftiek
	aftiek.position = position
	remove_child(aftiek)
	Handle.actualLevel.add_child(aftiek)
	queue_free()
