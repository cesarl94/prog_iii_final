extends Sprite

var HP = 25
var Exploto = false
var NumAnim = 0
export(PackedScene) var Chispa1 = null
export(PackedScene) var Chispa2 = null

func _ready():
	$Area2D.set_meta("tc","pc")
	$Area2D.set_meta("p",self)

func _kill():
	if Exploto: return
	Handle.sound.play_sound("computadora")
	Handle.sound.stop_sound("bomba")
	Exploto = true
	$Timer1.start()
	Handle.shakeCameraBegin(1.35)
	$Timer2.wait_time = 0.1
	$Timer2.start()

func _on_Timer1_timeout():
	if frame == 1: return
	frame += 1
	for c in get_children():
		if not c is Timer:
			if Handle.distancia(c.position,Vector2()) > 1.0:
				c._parent_button_press()
	Handle.actualLevel._apagarLuces()


func _on_Timer2_timeout():
	match NumAnim:
		0:
			crear_chispa(1, position - Vector2(16, 2), true)
			crear_chispa(2, position + Vector2(8, -10))
			crear_chispa(1, position + Vector2(1, 6), true)
			$Timer2.wait_time = 0.3
		1:
			crear_chispa(2, position - Vector2(8, 13), false, false)
			crear_chispa(1, position + Vector2(16, -2), true)
			crear_chispa(1, position - Vector2(16, -6), true)
		2:
			crear_chispa(1, position - Vector2(8, -14), true)
			crear_chispa(2, position + Vector2(8, 6))
			crear_chispa(2, position - Vector2(0, 21), false, false)
			
		3:
			crear_chispa(2, position + Vector2(-8, 6))
			crear_chispa(1, position + Vector2(16, 14), true, false)
			crear_chispa(1, position + Vector2(8, -10), true, false)
		4:
			crear_chispa(2, position - Vector2(16, 2), false, true, 0.1)
			crear_chispa(1, position + Vector2(0, 14), true, false, 0.1)
			crear_chispa(2, position + Vector2(16, -21), false, false, 0.1)
		
	NumAnim += 1
	$Timer2.start()

func crear_chispa(num, posicion, flipear = false, palante = true, extiende = 0.1):
	var chispita = null
	if num == 2: chispita = Chispa2.instance()
	elif num == 1: chispita = Chispa1.instance()
	chispita._play(posicion, flipear, palante, extiende)
	Handle.actualLevel.add_child(chispita)


func _on_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "flecha": return
	var flecha = body.get_meta("p")
	if not flecha.efectiva: return
	flecha._activar()
	HP -= 1
	if HP == 0:
		_kill()


















