extends Node2D

var usado = false

func _on_Aftiek_area_entered(area):
	if usado or not area.has_meta("tc") or area.get_meta("tc") != "v": return
	usado = true
	get_node("Aftiek")._iniciar_conversacion(area.get_meta("p").color)
