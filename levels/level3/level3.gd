extends Node2D

export(int) var TILES_X = 51
export(int) var TILES_Y = 50
export(String) var PASSWORD = "TLPT"
var PAUSAR_AL_COMENZAR = false

func _ready():
	if Engine.editor_hint: return
	for d in get_node("Collisions").get_children():
		for b in d.get_children():
			b.get_children().front().set_meta("tc", d.get_name()) #tc: tipo colisión, se almacenan como "Up", "Down", "Left" y "Rigth"
			b.set_collision_mask_bit(0,0)
			b.set_collision_layer_bit(0,0)
			b.set_collision_layer_bit(Handle._get_mask_from_dir(d.get_name()),1)
	for e in get_node("Escaleras").get_children():
		if not e is Area2D: continue
		e.set_meta("tc", "escalera")
		var ab = AABB(Vector3(1000000, 1000000, 0), Vector3(0, 0, 0))
		for v in e.get_children().front().polygon:
			if v.x < ab.position.x: ab.position.x = v.x
			if v.y < ab.position.y: ab.position.y = v.y
			if v.x > ab.size.x: ab.size.x = v.x
			if v.y > ab.size.y: ab.size.y = v.y
		ab.size.x = ab.size.x - ab.position.x
		ab.size.y = ab.size.y - ab.position.y
		e.set_meta("aabb",ab)
	
	Handle.intercambiar_info_level($Vikingos, self)
	Handle.sound.play_music("space")
	
	if Handle.main.android:
		"""var preg = $Misiones/preg
		$Misiones.remove_child(preg)
		preg.queue_free()
		preg = $Misiones/preg7
		$Misiones.remove_child(preg)
		preg.queue_free()"""
		
		$Misiones/preg/Conversacion/GloboText._change_text("dave el veloz tiene una-cabeza dura. puede golpear-a los enemigos y derribar-ciertas barreras presionando-'a' mientras corre.")
		$Misiones/preg2/Conversacion/GloboText._change_text("para operar el-teletransportador,-párese dentro y-presione 'c'.")
		$Misiones/preg3/Conversacion/GloboText._change_text("eddy el robusto puede usar-su escudo como paraCaidas-mientras cae. coloque el-escudo sobre su cabeza-presionando 'a' o 'b'.")
		$Misiones/preg4/Conversacion/GloboText._change_text("para desbloquear la cerradura,-se debe utilizar la llave-correcta. párese junto a la -cerradura, y presione la-llave desde el inventario.")

		

func _process(delta):
	if Engine.editor_hint: return
	if PAUSAR_AL_COMENZAR:
		
		PAUSAR_AL_COMENZAR = false
		get_tree().paused = true
	set_process(false)
