extends Node2D

export(bool)var activado = false

var i = 0

func _ready():
	if activado:
		_play()

func _():
	for c in get_children():
		if c is Sprite:
			if i % 3 != 2: c.frame += 1
			else: c.frame -= 2
	i += 1

func _get_state():
	return $AnimationPlayer.is_playing()

func _stop():
	$AnimationPlayer.stop()

func _play():
	for c in get_children():
		if c is Sprite:
			c.frame -= i % 3
	i = 0
	$AnimationPlayer.play("Idle")