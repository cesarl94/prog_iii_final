extends Node2D

var colorV

func _ready():
	set_process(false)
	set_physics_process(false)
	z_index += -get_parent().z_index

func _process(delta):
	if not Handle._esperar_tecla(): return
	var siguiente = false
	var globo = null
	for c in get_children():
		if not siguiente:
			if c.is_visible_in_tree():
				c.hide()
				siguiente = true
		else:
			globo = c
			break
	if globo != null:
		globo.show()
		if Handle._es_gris(globo._get_color()):
			globo._change_color(colorV)
	else:
		set_process(false)
		get_tree().paused = false
		get_parent()._terminada_conversacion()
		pause_mode = PAUSE_MODE_INHERIT
	

func _iniciar_conversacion(colorVikingo):
	if get_children().empty(): return
	colorV = colorVikingo
	set_process(true)
	get_tree().paused = true
	var primer_globo = get_children().front()
	primer_globo.show()
	if Handle._es_gris(primer_globo._get_color()):
		primer_globo._change_color(colorV)
	pause_mode = PAUSE_MODE_PROCESS
	Handle.sound.play_sound("globo")
	