extends Node

var vistaJuego
var nivel
var laotra

func _ready():
	set_process(false)

func _activar(VistaJuego, Nivel):
	vistaJuego = VistaJuego
	nivel = Nivel
	#vistaJuego.hide()
	set_process(true)
	laotra = true
	

func _process(delta):
	if laotra:
		laotra = false
		return
	get_tree().paused = true
	vistaJuego._load_level(nivel)
	set_process(false)
	vistaJuego.show()
	get_tree().paused = false
	Handle.camara.current = true
	_desactivar()

func _desactivar():
	Handle.main.modulate = 0
	Handle.camara.position = Handle.vikingoSelec.position
	Handle.camara.reset_smoothing()