extends Node

export(float) var volume = 0
var volume_base = 1
var state = 0
var opcion = 0
var password = [0, 0, 0, 0]
var posp = 0
signal go_to_lv


var shakeCamera = false
var nivel_arranca

var hay_partida = false
var nombre_partidas = []
var offset_partidas = 0

class _AABB:
	var position = Vector2()
	var size = Vector2()

func esta_dentro(aabb,pos):
	return pos.x >= aabb.position.x and pos.x < aabb.position.x + aabb.size.x and pos.y >= aabb.position.y and pos.y < aabb.position.y + aabb.size.y

func _ready():
	Handle.sound.play_music("intro")
	
	set_process(false)
	set_process_input(false)
	
	
	var file = File.new()
	if file.file_exists("user://play.data"):
		hay_partida = true
	if hay_partida:
		$Menu/BitmapFont.Texto = "nuevo juego---sonido--- continuar---   salir"
	else:
		$Menu/BitmapFont.Texto = "nuevo juego---sonido---   salir"
	
	
	for c in $Texto1.get_children():
		c.create()
	for c in $Menu.get_children():
		if not c is Sprite and not c is TouchScreenButton: c.create()
	for c in $Password.get_children():
		if not c is Sprite and not c is TouchScreenButton: c.create()
	if hay_partida:
		crear_partidas()



func _process(delta):
	if state == 0:
		if shakeCamera:
			$Camera2D.position.x += rand_range(-10, 10)
			$Camera2D.position.y += rand_range(-10, 10)
	elif state == 5:
		var volumen = volume_base + volume - 1.0
		Handle.sound.set_sounds_volume(volumen)
		Handle.sound.set_music_volume(volumen)



func _on_animation_finished(anim_name):
	if anim_name == "FadeOut":
		Handle.sound.stop_music()
		emit_signal("go_to_lv",nivel_arranca)
	elif anim_name == "Play":
		set_process(true)
		set_process_input(true)
		$Texto2/BitmapFont.create()
		$Timer4.start()
		state = 1
		

func set_state2():
	state = 2
	set_process_input(true)

func get_char_index(i):
	match i:
		0: return " "
		1: return "B"
		2: return "C"
		3: return "D"
		4: return "F"
		5: return "G"
		6: return "H"
		7: return "J"
		8: return "K"
		9: return "L"
		10: return "M"
		11: return "N"
		12: return "P"
		13: return "Q"
		14: return "R"
		15: return "S"
		16: return "T"
		17: return "V"
		18: return "W"
		19: return "X"
		20: return "Y"
		21: return "Z"
		22: return "0"
		23: return "1"
		24: return "2"
		25: return "3"
		26: return "4"
		27: return "5"
		28: return "6"
		29: return "7"
		30: return "8"
		31: return "9"
		32: return "!"

func get_nivel_password(codigo):
	match codigo:
		"STRT": return 1
		"GR8T": return 2
		"TLPT": return 3
		"GRND": return 4
	return -1

func _on_Timer_timeout():
	if state == 4:
		for c in $Password.get_children():
			if c.is_visible_in_tree():
				c.hide()
			else:
				c.show()
		$Menu.show()
		$Password.hide()
		state = 2
		return
	if state == 3:
		if Input.is_action_pressed("DOWN"):
			letra_password_abajo()
		elif Input.is_action_pressed("UP"):
			letra_password_arriba()

func letra_password_abajo():
	password[posp] -= 1
	if password[posp] < 0:
		password[posp] = 32
	$Password/BitmapFont2.Texto = ""
	for i in 4:
		$Password/BitmapFont2.Texto += get_char_index(password[i])
	$Password/BitmapFont2.create()
	$Timer.wait_time = 0.15
	$Timer.start()
	Handle.sound.play_sound("menu")

func letra_password_arriba():
	password[posp] += 1
	if password[posp] > 32:
		password[posp] = 0
	$Password/BitmapFont2.Texto = ""
	for i in 4:
		$Password/BitmapFont2.Texto += get_char_index(password[i])
	$Password/BitmapFont2.create()
	$Timer.wait_time = 0.15
	$Timer.start()
	Handle.sound.play_sound("menu")

func shake():
	set_process(true)
	shakeCamera = true
	$Timer2.start()
	Handle.sound.play_sound("cae_pesado")

var primera = true
func _on_Timer2_timeout():
	if primera:
		set_process(false)
		primera = false
	shakeCamera = false
	$Camera2D.position.x = 320
	$Camera2D.position.y = 224
	

func _on_Timer3_timeout():
	$AnimationPlayer.play("FadeOut")
	state = 5

var drag_partida
var presiona = 0
var pos_presiona
var scrollea
var scroll_scale
var scroll_active
var scroll_aabb
var scroll_moving

func _input(event) :
	if state == 1 and ((event is InputEventKey and event.pressed) or (event is InputEventScreenTouch and not event.pressed)):
		set_process_input(false)
		$AnimationPlayer.play("ApareceMenu")
		$Timer4.stop()
		$Texto2.hide()
	elif event is InputEventScreenDrag:
		if state == 2: presiona = 0
		elif state == 3:
			if scrollea:
				drag_partida = true
				var offset_antes = offset_partidas
				if pos_presiona.y - event.position.y > 16 and offset_partidas + 5 < nombre_partidas.size():
					offset_partidas += 1
					pos_presiona.y -= 16
					$partidas/Node2D.position.y -= 24
				if pos_presiona.y - event.position.y < -16 and offset_partidas > 0:
					offset_partidas -= 1
					pos_presiona.y += 16
					$partidas/Node2D.position.y += 24
				if offset_antes != offset_partidas:
					Handle.sound.play_sound("menu")
					$partidas.Texto = ""
					for j in 5:
						if offset_partidas + j >= nombre_partidas.size(): break
						$partidas.Texto += nombre_partidas[offset_partidas + j] + "---"
					$partidas.create()
					
					if offset_partidas == nombre_partidas.size() - 5:
						$ColorRect/ColorRect.rect_position.y = $ColorRect.rect_size.y - $ColorRect/ColorRect.rect_size.y
					else:
						$ColorRect/ColorRect.rect_position.y = offset_partidas * scroll_scale
			elif scroll_moving:
				var movio = pos_presiona.y - event.position.y
				if abs(movio) > scroll_scale:
					var veces = int(movio / scroll_scale)
					var offset_antes = offset_partidas
					offset_partidas -= veces
					
					if offset_partidas + 5 > nombre_partidas.size():
						offset_partidas = nombre_partidas.size() - 5
					if offset_partidas < 0:
						offset_partidas = 0
					if offset_antes != offset_partidas:
						Handle.sound.play_sound("menu")
						var diferencia = offset_partidas - offset_antes
						pos_presiona.y += diferencia * scroll_scale
						$partidas/Node2D.position.y -= 24 * diferencia
						
						$partidas.Texto = ""
						for j in 5:
							if offset_partidas + j >= nombre_partidas.size(): break
							$partidas.Texto += nombre_partidas[offset_partidas + j] + "---"
						$partidas.create()
						
						if offset_partidas == nombre_partidas.size() - 5:
							$ColorRect/ColorRect.rect_position.y = $ColorRect.rect_size.y - $ColorRect/ColorRect.rect_size.y
						else:
							$ColorRect/ColorRect.rect_position.y = offset_partidas * scroll_scale
						
			
	elif event is InputEventScreenTouch and state == 3:
		if event.pressed:
			pos_presiona = event.position
			var aabb = _AABB.new()
			aabb.position = $ColorRect.rect_position
			aabb.size = $ColorRect.rect_size
			
			scrollea = esta_dentro(aabb,event.position)
			
			if scroll_active:
				aabb.position += $ColorRect/ColorRect.rect_position
				aabb.size = $ColorRect/ColorRect.rect_size
				
				scroll_moving = esta_dentro(aabb,event.position)
				if scroll_moving: 
					scrollea = false
					scroll_aabb = aabb
					$ColorRect/ColorRect.color = Color(0.27, 0.1, 0.1)
		else:
			$ColorRect/ColorRect.color = Color(0.27, 0.27, 0.27)
			scroll_moving = false
	
	



func touch_menu(boton, valor):
	if state != 2: return
	if valor:
		presiona = boton
	elif presiona != 0:
		presiona -= 1
		match presiona:
			0:
				$Timer3.start()
				Handle.sound.play_sound("menu_enter")
				nivel_arranca = 0
			1:
				var fuente = get_node("Menu/BitmapFont2")
				Handle.sound.play_sound("menu")
				var actualState = fuente.Texto
				if actualState == "on":
					fuente.Texto = "off"
					Handle.sound.set_volume(0)
					Handle.volumen = false
					volume_base = 0
				else:
					fuente.Texto = "on"
					Handle.sound.set_volume(1)
					Handle.volumen = true
					volume_base = 1
				fuente.create()
			2:
				if hay_partida:
					$Menu.hide()
					$partidas.show()
					$ColorRect.show()
					state = 3
					Handle.sound.play_sound("menu")
					
					$partidas.Texto = ""
					for j in 5:
						if offset_partidas + j >= nombre_partidas.size(): break
						$partidas.Texto += nombre_partidas[offset_partidas + j] + "---"
					$partidas.create()
				else:
					get_tree().quit()
			3:
				if hay_partida:
					get_tree().quit()

func crear_partidas():
	var file = File.new()
	file.open("user://play.data", File.READ)
	var levels = file.get_32()
	file.close()
	nombre_partidas.push_back("volver")
	$partidas.position = $Menu/BitmapFont.position + Vector2(32,-16)
	$partidas.scale = Vector2(2,2)
	
	var i = levels + 1
	while i > 1:
		if i <= levels:
			nombre_partidas.push_back("nivel " + String(i))
		
		var touch = TouchScreenButton.new()
		touch.normal = preload("res://imagenes/white.png")
		touch.scale = Vector2(1.25, 0.2)
		touch.position = Vector2(-7,-3 + 24*(levels - (i-1)))
		$partidas/Node2D.add_child(touch)
		touch.modulate.a = 0.0
		touch.connect("pressed",self,"partida_press",[(levels - (i-1)), true])
		touch.connect("released",self,"partida_press",[(levels - (i-1)), false])
		i -= 1
	
	scroll_scale = $ColorRect.rect_size.y / levels
	$ColorRect/ColorRect.rect_size.y = 5 * scroll_scale
	if $ColorRect/ColorRect.rect_size.y > $ColorRect.rect_size.y:
		$ColorRect/ColorRect.rect_size.y = $ColorRect.rect_size.y
	scroll_active = levels > 5
	scroll_moving = false
	if not scroll_active:
		$ColorRect.rect_size.x -= 32
		$ColorRect/ColorRect.hide()
		
		
	


func partida_press(num_partida, valor):
	if state != 3: return
	
	if valor:
		drag_partida = false
		presiona = num_partida
	else:
		if not drag_partida and num_partida >= offset_partidas and num_partida < offset_partidas + 5:
			
			if num_partida == 0:
				$Menu.show()
				$partidas.hide()
				$ColorRect.hide()
				state = 2
			else:
				var contador = 0
				var level = ""
				for c in nombre_partidas[num_partida]:
					if contador >= 6:
						level += c
					contador += 1
				nivel_arranca = int(level)
				Handle.sound.play_sound("menu_enter")
				$Timer3.start()
	
	


func _on_Timer4_timeout():
	if $Texto2.is_visible_in_tree(): $Texto2.hide()
	else: $Texto2.show()














