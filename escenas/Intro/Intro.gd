extends Node

export(float) var volume = 0
var volume_base = 1
var state = 0
var opcion = 0
var password = [0, 0, 0, 0]
var posp = 0
signal go_to_lv


var shakeCamera = false
var nivel_arranca

var hay_partida = false
var nombre_partidas = []
var offset_partidas = 0
var pos_partidas

func _ready():
	Handle.sound.play_music("intro")
	
	set_process(false)
	set_process_input(false)
	
	
	var file = File.new()
	if file.file_exists("user://play.data"):
		hay_partida = true
	if hay_partida:
		$Menu/BitmapFont.Texto = "nuevo juego--sonido--continuar--salir"
	else:
		$Menu/BitmapFont.Texto = "nuevo juego--sonido--salir"
	
	
	for c in $Texto1.get_children():
		c.create()
	for c in $Menu.get_children():
		if not c is Sprite: c.create()
	for c in $Password.get_children():
		if not c is Sprite: c.create()
	if hay_partida:
		crear_partidas()
	

func _process(delta):
	if Handle._esperar_tecla():
		if state == 2:
			match opcion:
				0:
					$Timer3.start()
					Handle.sound.play_sound("menu_enter")
					nivel_arranca = 1
				1:
					var fuente = get_node("Menu/BitmapFont2")
					Handle.sound.play_sound("menu")
					var actualState = fuente.Texto
					if actualState == "on":
						fuente.Texto = "off"
						Handle.sound.set_volume(0)
						Handle.volumen = false
						volume_base = 0
					else:
						fuente.Texto = "on"
						Handle.sound.set_volume(1)
						Handle.volumen = true
						volume_base = 1
					fuente.create()
				2:
					if hay_partida:
						$Menu.hide()
						$Sprite2.show()
						$partidas.show()
						state = 3
						Handle.sound.play_sound("menu")
						
						pos_partidas = 1
						$partidas.Texto = ""
						for j in 6:
							if offset_partidas + j >= nombre_partidas.size(): break
							$partidas.Texto += nombre_partidas[offset_partidas + j] + "--"
						$partidas.create()
					else:
						get_tree().quit()
				3:
					get_tree().quit()
					
		elif state == 3:
			if pos_partidas == 0:
				$Menu.show()
				$Sprite2.hide()
				$partidas.hide()
				state = 2
			else:
				var contador = 0
				var level = ""
				for c in nombre_partidas[pos_partidas]:
					if contador >= 6:
						level += c
					contador += 1
				nivel_arranca = int(level)
				Handle.sound.play_sound("menu_enter")
				$Timer3.start()
	if state == 0:
		if shakeCamera:
			$Camera2D.position.x += rand_range(-10, 10)
			$Camera2D.position.y += rand_range(-10, 10)
	elif state == 2:
		if Input.is_action_just_pressed("DOWN"):
			if (hay_partida and opcion < 3) or (not hay_partida and opcion < 2):
				opcion += 1
				$Menu/Sprite.position.y += 32
			else:
				
				$Menu/Sprite.position.y -= 32 * opcion
				opcion = 0
			Handle.sound.play_sound("menu")
		elif Input.is_action_just_pressed("UP"):
			if opcion > 0:
				opcion -= 1
				$Menu/Sprite.position.y -= 32
			else:
				if hay_partida:
					opcion = 3
				else:
					opcion = 2
				$Menu/Sprite.position.y += 32 * opcion
			Handle.sound.play_sound("menu")
	elif state == 3:
		var pos_partidas_antes = pos_partidas
		if Input.is_action_just_pressed("DOWN"):
			pos_partidas += 1
			if pos_partidas >= nombre_partidas.size():
				pos_partidas = 0
			Handle.sound.play_sound("menu")
		if Input.is_action_just_pressed("UP"):
			pos_partidas -= 1
			if pos_partidas < 0:
				pos_partidas = nombre_partidas.size() - 1
			Handle.sound.play_sound("menu")
		var offset_antes = offset_partidas
		if pos_partidas == offset_partidas and offset_partidas > 0:
			offset_partidas -= 1
		elif pos_partidas == offset_partidas + 5 and nombre_partidas.size() - 1 > offset_partidas + 5:
			offset_partidas += 1
		elif pos_partidas < offset_partidas:
			offset_partidas = pos_partidas
		elif pos_partidas > offset_partidas + 5:
			offset_partidas = pos_partidas - 5
			
		if offset_antes != offset_partidas:
			$partidas.Texto = ""
			for j in 6:
				if offset_partidas + j >= nombre_partidas.size(): break
				$partidas.Texto += nombre_partidas[offset_partidas + j] + "--"
			$partidas.create()
		if pos_partidas_antes != pos_partidas:
			$Sprite2.position.y = $partidas.position.y + (pos_partidas - offset_partidas) * 32 + 4
		
	elif state == 5:
		var volumen = volume_base + volume - 1.0
		Handle.sound.set_sounds_volume(volumen)
		Handle.sound.set_music_volume(volumen)



func _on_animation_finished(anim_name):
	if anim_name == "FadeOut":
		Handle.sound.stop_music()
		emit_signal("go_to_lv",nivel_arranca)
	elif anim_name == "Play":
		set_process(true)
		set_process_input(true)
		$Texto2/BitmapFont.create()
		$Timer4.start()
		state = 1
		

func set_state2():
	state = 2

func get_char_index(i):
	match i:
		0: return " "
		1: return "B"
		2: return "C"
		3: return "D"
		4: return "F"
		5: return "G"
		6: return "H"
		7: return "J"
		8: return "K"
		9: return "L"
		10: return "M"
		11: return "N"
		12: return "P"
		13: return "Q"
		14: return "R"
		15: return "S"
		16: return "T"
		17: return "V"
		18: return "W"
		19: return "X"
		20: return "Y"
		21: return "Z"
		22: return "0"
		23: return "1"
		24: return "2"
		25: return "3"
		26: return "4"
		27: return "5"
		28: return "6"
		29: return "7"
		30: return "8"
		31: return "9"
		32: return "!"

func get_nivel_password(codigo):
	match codigo:
		"STRT": return 1
		"GR8T": return 2
		"TLPT": return 3
		"GRND": return 4
	return -1

func _on_Timer_timeout():
	if state == 4:
		for c in $Password.get_children():
			if c.is_visible_in_tree():
				c.hide()
			else:
				c.show()
		$Menu.show()
		$Password.hide()
		state = 2
		return
	if state == 3:
		if Input.is_action_pressed("DOWN"):
			letra_password_abajo()
		elif Input.is_action_pressed("UP"):
			letra_password_arriba()

func letra_password_abajo():
	password[posp] -= 1
	if password[posp] < 0:
		password[posp] = 32
	$Password/BitmapFont2.Texto = ""
	for i in 4:
		$Password/BitmapFont2.Texto += get_char_index(password[i])
	$Password/BitmapFont2.create()
	$Timer.wait_time = 0.15
	$Timer.start()
	Handle.sound.play_sound("menu")

func letra_password_arriba():
	password[posp] += 1
	if password[posp] > 32:
		password[posp] = 0
	$Password/BitmapFont2.Texto = ""
	for i in 4:
		$Password/BitmapFont2.Texto += get_char_index(password[i])
	$Password/BitmapFont2.create()
	$Timer.wait_time = 0.15
	$Timer.start()
	Handle.sound.play_sound("menu")

func shake():
	set_process(true)
	shakeCamera = true
	$Timer2.start()
	Handle.sound.play_sound("cae_pesado")

var primera = true
func _on_Timer2_timeout():
	if primera:
		set_process(false)
		primera = false
	shakeCamera = false
	$Camera2D.position.x = 320
	$Camera2D.position.y = 224
	

func _on_Timer3_timeout():
	$AnimationPlayer.play("FadeOut")
	state = 5

func _input(event) :
	if (event is InputEventKey and event.pressed) or (event is InputEventScreenTouch and not event.pressed):
		set_process_input(false)
		$AnimationPlayer.play("ApareceMenu")
		$Timer4.stop()
		$Texto2.hide()
	





func crear_partidas():
	var file = File.new()
	file.open("user://play.data", File.READ)
	var levels = file.get_32()
	file.close()
	nombre_partidas.push_back("volver")
	var i = levels
	while i > 1:
		nombre_partidas.push_back("nivel " + String(i))
		i -= 1
	$partidas.position = $Menu/BitmapFont.position
	$partidas.scale = Vector2(2,2)
	
	




func _on_Timer4_timeout():
	if $Texto2.is_visible_in_tree(): $Texto2.hide()
	else: $Texto2.show()

