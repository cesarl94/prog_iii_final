extends Node2D

export (String) var texto = ""
export (String, "GRIS","AZUL","ROJO","MARRON","VERDE","AZUL OSCURO") var COLOR_FONDO_S = "GRIS"
export (bool) var USAR_PIQUITO = false
export(bool)var PIQUITO_ARRIBA = false
export(int)var POSITION_PIQUITO = 0
export(bool)var CENTRAR = 0


export (Texture) var TexturaBalloon = null
export (Texture) var TexturaUp = null
export (Texture) var TexturaDown = null
export (Texture) var TexturaLeft = null
export (Texture) var TexturaRight = null

var size
var fondo
var COLOR_FONDO

func _ready():
	update()

func update():
	
	match COLOR_FONDO_S:
		"GRIS": COLOR_FONDO = Color(0.5, 0.5, 0.5)
		"AZUL": COLOR_FONDO = Color(0.0, 0.28235, 0.70588)
		"ROJO": COLOR_FONDO = Color(0.84706, 0.0, 0.0)
		"MARRON": COLOR_FONDO = Color(0.70588, 0.70588, 0.0)
		"VERDE": COLOR_FONDO = Color(0.0, 0.70588, 0.0)
		"AZUL OSCURO": COLOR_FONDO = Color(0.0, 0.0, 0.28235)
	
	$BitmapFont.Texto = texto
	size = $BitmapFont.create()
	$BitmapFont.z_index = 1
	var sprite
	#esquina superior izquierda
	sprite = Sprite.new()
	sprite.centered = false
	sprite.position = Vector2()
	sprite.texture = TexturaBalloon
	sprite.hframes = 4
	sprite.vframes = 2
	sprite.frame = 0
	sprite.z_index = 1
	$Sprites.add_child(sprite)
	#parte superior media
	sprite = Sprite.new()
	sprite.centered = false
	sprite.position = Vector2(8, 0)
	sprite.texture = TexturaUp
	sprite.region_rect = Rect2(0,0,size.x,8)
	sprite.region_enabled = true
	sprite.z_index = 1
	$Sprites.add_child(sprite)
	#esquina superior derecha
	sprite = Sprite.new()
	sprite.centered = false
	sprite.position = Vector2(size.x + 8, 0)
	sprite.texture = TexturaBalloon
	sprite.hframes = 4
	sprite.vframes = 2
	sprite.frame = 1
	sprite.z_index = 1
	$Sprites.add_child(sprite)
	#parte derecha media
	sprite = Sprite.new()
	sprite.centered = false
	sprite.position = Vector2(size.x + 8, 8)
	sprite.texture = TexturaRight
	sprite.region_rect = Rect2(0,0,8,size.y)
	sprite.region_enabled = true
	sprite.z_index = 1
	$Sprites.add_child(sprite)
	#esquina inferior derecha
	sprite = Sprite.new()
	sprite.centered = false
	sprite.position = Vector2(size.x + 8, size.y + 8)
	sprite.texture = TexturaBalloon
	sprite.hframes = 4
	sprite.vframes = 2
	sprite.frame = 5
	sprite.z_index = 1
	$Sprites.add_child(sprite)
	#parte inferior media
	sprite = Sprite.new()
	sprite.centered = false
	sprite.position = Vector2(8, size.y + 8)
	sprite.texture = TexturaDown
	sprite.region_rect = Rect2(0,0,size.x,8)
	sprite.region_enabled = true
	sprite.z_index = 1
	$Sprites.add_child(sprite)
	#esquina inferior izquierda
	sprite = Sprite.new()
	sprite.centered = false
	sprite.position = Vector2(0, size.y + 8)
	sprite.texture = TexturaBalloon
	sprite.hframes = 4
	sprite.vframes = 2
	sprite.frame = 4
	sprite.z_index = 1
	$Sprites.add_child(sprite)
	#parte izquierda media
	sprite = Sprite.new()
	sprite.centered = false
	sprite.position = Vector2(0, 8)
	sprite.texture = TexturaLeft
	sprite.region_rect = Rect2(0,0,8,size.y)
	sprite.region_enabled = true
	sprite.z_index = 1
	$Sprites.add_child(sprite)
	
	if USAR_PIQUITO:
		sprite = Sprite.new()
		sprite.centered = false
		sprite.texture = TexturaBalloon
		sprite.hframes = 4
		
		if PIQUITO_ARRIBA:
			sprite.position = Vector2(8 + POSITION_PIQUITO * 8, -8)
			sprite.vframes = 2
			sprite.frame = 3
		else:
			sprite.position = Vector2(8 + POSITION_PIQUITO * 8, size.y + 8)
			sprite.vframes = 1
			sprite.frame = 2
		sprite.z_index = 1
		$Sprites.add_child(sprite)
	
	fondo = ColorRect.new()
	fondo.rect_size  = size + Vector2(10,10)
	fondo.rect_position  = Vector2(3,3)
	fondo.color = COLOR_FONDO
	$Sprites.add_child(fondo)
	
	
	if CENTRAR:
		$Sprites.position = Vector2(-(size.x + 16) / 2.0,-(size.y + 16) / 2.0)
		$BitmapFont.position = $Sprites.position + Vector2(8,8)
	else:
		$BitmapFont.position = Vector2(8,8)


func _change_text(textoNuevo):
	if textoNuevo == texto: return
	texto = textoNuevo
	$BitmapFont.Texto = textoNuevo
	$BitmapFont.create()
	for c in $Sprites.get_children():
		c.queue_free()
	update()

func _change_color(colorNuevo):
	if colorNuevo == COLOR_FONDO: return
	COLOR_FONDO = colorNuevo
	fondo.color = colorNuevo

func _get_color():
	return COLOR_FONDO