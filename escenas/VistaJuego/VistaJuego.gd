extends ViewportContainer

var nivel
var reiniciadas = 0

func _ready():
	Handle.pausa = $Viewport/Pausa
	
	$Viewport/Password.connect("end_password", Handle, "_end_password")
	$Viewport/Filtro.mate = material
	
	if get_parent().android:
		Handle.item = $Viewport/Item
	else:
		var item = $Viewport/Item
		$Viewport.remove_child(item)
		item.queue_free()
	

func aniade_level_nuevo():
	var file = File.new()
	if file.file_exists("user://play.data"):
		file.open("user://play.data",File.READ)
		if file.get_32() < nivel:
			file.close()
			file.open("user://play.data",File.WRITE_READ)
			file.store_32(nivel)
			file.close()
			return true
		file.close()
		return false
	file.open("user://play.data",File.WRITE_READ)
	file.store_32(nivel)
	file.close()
	return true

func _load_level(num):
	nivel = num
	private_load_level()
	
	
	
	if nivel > 1 and aniade_level_nuevo():
		Handle.actualLevel.PAUSAR_AL_COMENZAR = true
		$Viewport/Password._mostrar()
	

func _reiniciar_level():
	Handle.escena_sigue = Handle.Escena.Nivel
	reiniciadas += 1
	Handle.main.get_node("Espera")._activar(self, nivel)
	

func private_load_level():
	if nivel == 1 and reiniciadas == 0:
		if $Viewport.has_node("level"):
			var nivel = $Viewport.get_node("level")
			$Viewport.remove_child(nivel)
			nivel.queue_free()
		
		$Viewport/ViewportTablero.show()
		$Viewport/ViewportGame.show()
		Handle.sound.stop_music()
		
	Handle.enemigos.resize(0)
	var rutaLevel = "res://levels/level" + String(nivel) + "/level" + String(nivel) + ".tscn"
	var level = load(rutaLevel).instance()
	level.name = "level"
	
	if nivel == 0:
		$Viewport.add_child(level)
		$Viewport/ViewportTablero.hide()
		$Viewport/ViewportGame.hide()
		Handle._end_password()
		if get_parent().android:
			get_parent().get_node("Control").activar_level0()
	else:
		$Viewport/ViewportGame/Viewport.add_child(level)
		
		if not $Viewport/ViewportTablero/Viewport.has_node("tablero"):
			var tablero
			if get_parent().android:
				tablero = preload("res://objetos/Tablero/Tablero_android.tscn").instance()
			else: tablero = preload("res://objetos/Tablero/Tablero.tscn").instance()
			tablero.name = "tablero"
			$Viewport/ViewportTablero/Viewport.add_child(tablero)
		else:
			$Viewport/ViewportTablero/Viewport.get_node("tablero")._reset()
		if get_parent().android:
			get_parent().get_node("Control").activar(true)
	
	material.set_shader_param("colorSuma",Vector3())
	
	









