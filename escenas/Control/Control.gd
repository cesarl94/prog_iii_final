extends Control

var comenzo = false
var direccionX = Handle.Direccion.QUIETO
var direccionY = Handle.Direccion.QUIETO
var _ultimo_activado = 0
export (bool) var activado = true setget activar
var in_level0 = false

func activar(nuevo_valor):
	in_level0 = false
	activado = nuevo_valor
	if not Engine.editor_hint and not is_inside_tree(): return
	if activado:
		for c in get_children():
			if c.name.find("Sprite2") == -1: c.show()
			else: c.hide()
	else:
		for c in get_children():
			c.hide()

func activar_level0():
	activar(false)
	in_level0 = true
	$Puntos.show()


func ready():
	comenzo = true
	$Circulo.position.x -=  Handle.main.rect_position.x
	$Puntos.position.x -= Handle.main.rect_position.x
	$A.position.x += Handle.main.rect_position.x
	$B.position.x += Handle.main.rect_position.x
	$C.position.x += Handle.main.rect_position.x
	var rotacion = 0
	for i in 8:
		var sprite0 = Sprite.new()
		sprite0.texture = load("res://escenas/Control/dir.png")
		sprite0.z_index = 100
		sprite0.rotation_degrees = rotacion
		sprite0.name = "Sprite1"+String(i)
		sprite0.position = $Circulo.position
		if not activado:
			sprite0.hide()
		var sprite1 = Sprite.new()
		sprite1.texture = load("res://escenas/Control/dir2.png")
		sprite1.hide()
		sprite1.z_index = 100
		sprite1.rotation_degrees = rotacion
		sprite1.name = "Sprite2"+String(i)
		sprite1.position = $Circulo.position
		add_child(sprite0)
		add_child(sprite1)
		rotacion -= 45
	

var apareciendo = false

func _process(delta):
	if not comenzo: 
		ready()
	if not activado: return
	if get_tree().paused:
		if $Circulo.is_visible_in_tree():
			$Circulo.hide()
			for c in get_children():
				if c.name.find("Sprite") != -1:
					c.hide()
	else:
		if not $Circulo.is_visible_in_tree():
			$Circulo.show()
			$Circulo.modulate.a = 0.0
			for c in get_children():
				if c.name.find("Sprite1") != -1:
					c.show()
					c.modulate.a = 0.0
			apareciendo = true
	if not apareciendo: return
	if $Circulo.modulate.a < 1.0:
		$Circulo.modulate.a += delta
		for c in get_children():
			if c.name.find("Sprite1") != -1:
				c.modulate.a += delta
	else:
		$Circulo.modulate.a = 1.0
		for c in get_children():
			if c.name.find("Sprite1") != -1:
				c.modulate.a = 1.0
		apareciendo = false
		

var last_pressed

func _input(event):
	if not activado: return
	if get_tree().paused: 
		if _ultimo_activado != 0:
			soltar_cruceta(false)
		if Handle.tablero.comerciando:
			event.position.x -= Handle.main.rect_position.x
			Handle.tablero._input(event)
		return
	if event is InputEventScreenTouch:
		if event.pressed:
			var distancia = Handle.distancia($Circulo.global_position,event.position)
			if distancia < 107:
				detectar_dir(event.position)
				last_pressed = event.index
		else:
			if _ultimo_activado != 0 and event.index == last_pressed:
				soltar_cruceta()
	if event is InputEventScreenDrag:
		if _ultimo_activado != 0:
			detectar_dir(event.position)
		else: 
			event.position.x -= Handle.main.rect_position.x
			Handle.tablero._input(event)
		
	

func soltar_cruceta(show = true):
	Input.action_release("UP")
	Input.action_release("DOWN")
	Input.action_release("LEFT")
	Input.action_release("RIGHT")
	if show:
		get_node("Sprite1"+String(_ultimo_activado - 1)).show()
	get_node("Sprite2"+String(_ultimo_activado - 1)).hide()
	_ultimo_activado = 0
	direccionX = Handle.Direccion.QUIETO
	direccionY = Handle.Direccion.QUIETO

func detectar_dir(posicion):
	var circulo_global = $Circulo.global_position
	var angulo = atan2((posicion.y - circulo_global.y), (posicion.x - circulo_global.x)) * 180.0 / PI
	var dirX = Handle.Direccion.QUIETO
	var dirY = Handle.Direccion.QUIETO
	
	if abs(angulo) < 67.5: dirX = Handle.Direccion.RIGHT
	elif abs(angulo) > 112.5: dirX = Handle.Direccion.LEFT
	if angulo < 0 and abs(angulo + 90) < 67.5:
		dirY = Handle.Direccion.UP
	elif angulo > 0 and abs(angulo - 90) < 67.5:
		dirY = Handle.Direccion.DOWN
	
	if dirX != direccionX:
		match dirX:
			Handle.Direccion.LEFT:
				Input.action_press("LEFT")
			Handle.Direccion.RIGHT:
				Input.action_press("RIGHT")
			Handle.Direccion.QUIETO:
				Input.action_release("LEFT")
				Input.action_release("RIGHT")
		direccionX = dirX
	if dirY != direccionY:
		match dirY:
			Handle.Direccion.UP:
				Input.action_press("UP")
			Handle.Direccion.DOWN:
				Input.action_press("DOWN")
			Handle.Direccion.QUIETO:
				Input.action_release("UP")
				Input.action_release("DOWN")
		direccionY = dirY
	var boton_num
	if angulo - 22.5 < 0:
		boton_num = int(abs(angulo - 22.5) / 45.0) + 1
	else: boton_num = 7 - int((angulo - 22.5) / 45.0) + 1
	
	if boton_num != _ultimo_activado:
		if _ultimo_activado != 0:
			get_node("Sprite1"+String(_ultimo_activado - 1)).show()
			get_node("Sprite2"+String(_ultimo_activado - 1)).hide()
		get_node("Sprite1"+String(boton_num - 1)).hide()
		get_node("Sprite2"+String(boton_num - 1)).show()
		_ultimo_activado = boton_num






func _on_A_pressed():
	if not activado: return
	Input.action_press("A")

func _on_A_released():
	if not activado: return
	Input.action_release("A")

func _on_B_pressed():
	if not activado: return
	Input.action_press("B")

func _on_B_released():
	if not activado: return
	Input.action_release("B")

func _on_C_pressed():
	if not activado: return
	Input.action_press("C")

func _on_C_released():
	if not activado: return
	Input.action_release("C")

func _on_Puntos_pressed():
	if not activado and not in_level0: return
	Input.action_press("ENTER")

func _on_Puntos_released():
	if not activado and not in_level0: return
	Input.action_release("ENTER")
	if not in_level0: return
	Handle.actualLevel.activar_fase3()
