extends Node2D

export(PackedScene) var Escena_muerte = null
export(int) var HP = 1

var entes_cercanos_monstruo = []
var entes_cercanos_base = []
var monstruo

func _ready():
	for c in get_children():
		if c is KinematicBody2D or c is Sprite:
			monstruo = c
			break
	
	var screen_area2 = $screen_area2
	remove_child(screen_area2)
	
	screen_area2.connect("area_entered", self, "_on_area_monstruo_entered")
	screen_area2.connect("area_exited", self, "_on_area_monstruo_exited")
	monstruo.add_child(screen_area2)
	if monstruo is KinematicBody2D:
		Handle.enemigos.push_back(self)
	

func _on_area_monstruo_entered(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "cam": return
	entes_cercanos_monstruo.push_back(meta) #pongo el texto y no el puntero ya q no hace falta
	if entes_cercanos_monstruo.size() == 1:
		_activar_monstruo(true)
	


func _on_area_monstruo_exited(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "cam": return
	entes_cercanos_monstruo.erase(meta) #pongo el texto y no el puntero ya q no hace falta
	if entes_cercanos_monstruo.empty():
		if entes_cercanos_base.empty():
			_activar_monstruo(false)
		elif monstruo is KinematicBody2D:
			monstruo._girar()
	

func _death():
	if Escena_muerte != null:
		var escena = Escena_muerte.instance()
		get_parent().add_child(escena)
		escena._activar(monstruo.global_position)
	Handle.enemigos.erase(self)
	queue_free()
	



func _on_screen_area_entered(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "cam": return
	entes_cercanos_base.push_back(meta) 
	if not monstruo.activo:
		_activar_monstruo(true)


func _on_screen_area_exited(area):
	if not area.has_meta("tc"): return
	var meta = area.get_meta("tc")
	if meta != "v" and meta != "cam": return
	entes_cercanos_base.erase(meta) 
	if entes_cercanos_monstruo.empty() and entes_cercanos_base.empty():
		_activar_monstruo(false)

func _activar_monstruo(valor):
	if valor == monstruo.activo: return
	monstruo._activar(valor)

