extends KinematicBody2D

signal area_i
signal area_o
signal bomb_i
signal bomb_o

func _ready():
	$CollisionShape2D.set_meta("tc","Down")

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	var vikingo = area.get_meta("p")
	vikingo.set_collision_mask_bit(4,1)
	emit_signal("area_i", vikingo)


func _on_area_exited(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	var vikingo = area.get_meta("p")
	if vikingo.state != Handle.State.Muriendo:
		vikingo.set_collision_mask_bit(4,0)
	emit_signal("area_o", vikingo)


func _on_bombas_body_entered(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "bomba": return
	emit_signal("bomb_i", body.get_meta("p"))


func _on_bombas_body_exited(body):
	if not body.has_meta("tc") or body.get_meta("tc") != "bomba": return
	emit_signal("bomb_o", body.get_meta("p"))




