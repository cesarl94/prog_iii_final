extends Node

export (float) var volumen_suma = 1
var volume = 1
var mutear = false

func get_string_hasta(cadena, hasta):
	var busqueda = cadena.find(hasta)
	if busqueda == -1: return
	var retorno = ""
	for i in busqueda:
		retorno += cadena[i]
	return retorno

func quitar_import(ruta):
	var retorno = ""
	for c in ruta:
		if c == '.':
			break
		retorno += c
	return retorno

func _ready():
	set_process(false)
	var dir = Directory.new()
	if dir.open("res://sounds/") == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while (file_name != ""):
			if file_name.find("import") != -1:
				var player = AudioStreamPlayer.new()
				$Node.add_child(player)
				player.name = quitar_import(file_name)
				player.stream = load("res://sounds/"+player.name + ".wav")
			file_name = dir.get_next()
	Handle.sound = self
	

func set_volume(volumen):
	
	set_sounds_volume(volumen)
	set_music_volume(volumen)
	volume = volumen

func agregar_volume(volumen):
	set_sounds_volume(volume + volumen - 1.0)
	set_music_volume(volume + volumen - 1.0)
	#volume = volumen

func play_sound(nombre):
	if mutear or not $Node.has_node(nombre): return
	$Node.get_node(nombre).play()

func stop_sound(nombre):
	if not $Node.has_node(nombre): return
	$Node.get_node(nombre).stop()

var anterior_musica = ""

func play_music(nombre):
	if mutear: return
	var musica
	if $Node.has_node("music"):
		musica = $Node.get_node("music")
	else:
		musica = AudioStreamPlayer.new()
		musica.name = "music"
		$Node.add_child(musica)
	
	if nombre == anterior_musica and is_music_playing(): return
	
	musica.stream = load("res://music/" + nombre + ".ogg")
	anterior_musica = nombre
	musica.play()

func is_music_playing():
	if not $Node.has_node("music"): return false
	return $Node.get_node("music").playing
	

func stop_music():
	if not $Node.has_node("music"): return
	$Node.get_node("music").stop()

func set_sounds_volume(volumen):
	for c in $Node.get_children():
		if c.name != "music":
			c.volume_db = -((1.0 - volumen) * 70.0)

func set_music_volume(volumen):
	if not $Node.has_node("music"): return
	$Node.get_node("music").volume_db = -((0 - volumen) * 70.0) - 70

func set_sound_volume(sonido, volumen):
	if not $Node.has_node(sonido): return
	$Node.get_node(sonido).volume_db = -((1.0 - volumen) * 70.0)

func _process(delta):
	agregar_volume(volumen_suma)