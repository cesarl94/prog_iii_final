extends Node2D

export (String) var Texto = ""
export (Texture) var Bitmap = null
export (Color) var color = Color(1,1,1)

func _get_index_char(letra):
	match letra:
		'A','a': return 0
		'B','b': return 1
		'C','c': return 2
		'D','d': return 3
		'E','e': return 4
		'F','f': return 5
		'G','g': return 6
		'H','h': return 7
		'I','i': return 8
		'J','j': return 9
		'K','k': return 10
		'L','l': return 11
		'M','m': return 12
		'N','n': return 13
		'O','o': return 14
		'P','p': return 15
		'Q','q': return 16
		'R','r': return 17
		'S','s': return 18
		'T','t': return 19
		'U','u': return 20
		'V','v': return 21
		'W','w': return 22
		'X','x': return 23
		'Y','y': return 24
		'Z','z': return 25
		',': return 26
		'.': return 27
		'!': return 28
		'\'','\"': return 29
		'?': return 30
		'0': return 31
		'1': return 32
		'2': return 33
		'3': return 34
		'4': return 35
		'5': return 36
		'6': return 37
		'7': return 38
		'8': return 39
		'9': return 40
		'ñ','Ñ': return 41
		'á','Á': return 42
		'é','É': return 43
		'í','Í': return 44
		'ó','Ó': return 45
		'ú','Ú': return 46
		'¿': return 48
		'¡': return 47
		
	return -1
		

func _ready():
	#create()
	set_process(false)
	set_physics_process(false)

func create():
	for c in get_children():
		if c is Sprite: c.queue_free()
	var offset = Vector2()
	var size = Vector2(0, 8)
	for c in Texto:
		if c != ' ' and c != '-' and c != '\n':#como no se permiten los saltos de línea, uso - para reemplazarlos
			var numFrame = _get_index_char(c)
			if numFrame != -1:
				var sprite = Sprite.new()
				sprite.scale = Vector2(0.5, 0.5)
				sprite.position = offset
				sprite.texture = Bitmap
				offset.x += 8
				if offset.x > size.x:
					size.x = offset.x
				sprite.hframes = 7
				sprite.vframes = 7
				sprite.centered = false
				sprite.frame = numFrame
				sprite.modulate = color
				add_child(sprite)
		else:
			if c == ' ': offset.x += 8
			else: 
				offset.x = 0
				offset.y += 8
				size.y += 8
	return size
