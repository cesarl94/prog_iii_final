extends Node2D

var state = 0
var opcion_reintentar = false
var titila = false
var vikingos  = []
export(float)var volumen_agrega = 1.0

signal to_menu
signal reiniciar

#func _ready():
#	_comenzar(false,false,true)

func _hay_alguno_muerto():
	return vikingos.find(false) != -1

func _primero_muerto():
	return vikingos.find(false)

func _comenzar(dave_con_vida, skyler_con_vida, eddy_con_vida):
	#Handle.main.modulate.a = 255
	vikingos.push_back(skyler_con_vida)
	vikingos.push_back(dave_con_vida)
	vikingos.push_back(eddy_con_vida)
	$AnimatedSprite.play("idle")
	$Barco.position = Vector2(384, 188)
	$GameOver.position = Vector2(320, 0)
	set_process(false)
	set_physics_process(false)
	if _hay_alguno_muerto():
		$Barco.show()
	if dave_con_vida: $Dave.show()
	if skyler_con_vida: $Skyler.show()
	if eddy_con_vida: $Eddy.show()
	
		

func _hide_barco():
	$Barco.hide()
	
func _aparece_cartel():
	$GloboText.show()
	$GloboText/Timer.start()

func _on_Timer_reintentar_timeout():
	if not titila:
		titila = true
		if not opcion_reintentar:
			$GloboText._change_text("¿reintentar?-        no")
		else:
			$GloboText._change_text("¿reintentar?-  si")
	else:
		titila = false
		$GloboText._change_text("¿reintentar?-  si    no")
		

func _process(delta):
	if Handle._esperar_tecla():
		if $GameOver.is_visible_in_tree():
			$AnimationPlayer.play("FadeOut")
		elif not $GloboText.is_visible_in_tree():
			_volver_al_juego()
		else:
			if not opcion_reintentar:
				_volver_al_juego()
			else:
				$GameOver.show()
				$GameOver/AnimationPlayer.play("Play")
				set_process(false)
				$GloboText.hide()
				$GloboText/Timer.stop()
				$TimerCartel.stop()
		
	if $GloboText.is_visible_in_tree():
		if (Input.is_action_just_pressed("LEFT") and opcion_reintentar) or (Input.is_action_just_pressed("RIGHT") and not opcion_reintentar):
			opcion_reintentar = not opcion_reintentar
			titila = false
			_on_Timer_reintentar_timeout()
			$GloboText/Timer.start()
		



func _volver_al_juego():
	_on_Timer_timeout()
	$TimerRayo.start()
	set_process(false)
	$GloboText.hide()
	$GloboText/Timer.stop()
	$TimerCartel.stop()


func _aparecer_vik():
	var primero = _primero_muerto()
	match primero:
		0: $Skyler.show()
		1: $Dave.show()
		2: $Eddy.show()
	vikingos[primero] = true




func _on_Timer_timeout():
	if _hay_alguno_muerto():
		match _primero_muerto():
			
			0: $Rayo.position = Vector2(238, 0)
			1: $Rayo.position = Vector2(110, 0)
			2: $Rayo.position = Vector2(-2, 0)
		$Rayo/AnimationPlayer.play("Play")
		Handle.sound.play_sound("rayo")
	else:
		$AnimationPlayer.play("FadeOut")
		$TimerRayo.stop()
		set_physics_process(true)


func _on_AnimationPlayer_finished(anim_name):
	if anim_name == "FadeIn":
		set_process(true)
		$Barco/Frames.play("Idle")
		$Barco/Movimiento.play("Idle")
		$TimerCartel.start()
		$TimerOlas.wait_time = 2.226
		_on_TimerOlas_timeout()
	elif anim_name == "FadeOut":
		if not opcion_reintentar: 
			emit_signal("reiniciar")
			Handle.main.modulate.a = 0
		else: 
			get_tree().paused = false
			Handle.escena_sigue = Handle.Escena.Intro
			emit_signal("to_menu")
		queue_free()


func _on_GameOver_animation_finished(anim_name):
	set_process(true)


func _on_TimerOlas_timeout():
	Handle.sound.play_sound("olas")
	if abs($TimerOlas.wait_time - 2.226) < 0.0001:
		$TimerOlas.wait_time = 3.226
	else:
		$TimerOlas.wait_time = 2.226
	$TimerOlas.start()

func _physics_process(delta):
	Handle.sound.agregar_volume(volumen_agrega)

func sonido_gameOver():
	Handle.sound.play_sound("cae_pesado")





