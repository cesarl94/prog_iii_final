extends Control

export(PackedScene) var proxima_escena

var is_loading = true

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Timer_timeout():
	if is_loading:
		$Timer.start()
		return
	get_parent().add_child(proxima_escena.instance())
	queue_free()
