extends Node2D

signal end_password

func _ready():
	set_physics_process(false)
	set_process(false)

func _process(delta):
	if Handle._esperar_tecla():
		hide()
		set_process(false)
		get_tree().paused = false
		emit_signal("end_password")
	

func _mostrar():
	if not is_visible_in_tree():
		show()
	set_process(true)
	