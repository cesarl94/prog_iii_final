extends Node2D

signal termina

func _ready():
	$AnimatedSprite.play("default")
	if Handle.volumen and not Handle.sound.mutear:
		$sonido.play()
	get_tree().paused = false
	if Handle.main.android:
		Handle.main.get_node("Control").activar(false)
		

func _on_animation_finished():
	emit_signal("termina")
	Handle.main.modulate.a = 0
	queue_free()
	if Handle.main.android:
		Handle.main.get_node("Control").activar(true)
