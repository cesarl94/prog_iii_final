extends Node

#Layers de colisión:
	
#0 techos
#1 pisos
#2 paredes a la izquierda
#3 paredes a la derecha
#4 ascensores
#5 dave
#6 skyler
#7 eddy
#8 flechas de skyler
#9 disparos (o todo lo que toca y lastima)
#10 enemigos
#11 escudo de eddy donde pararse
#12 escudo de eddy protector
#13 paredes invisibles para evitar que enemigos se caigan de las plataformas
#14 item touch

var GRAVITY = 241.1
var totalTime = 0.0
var totalTime2 = 0.0
var vikingoSN
var pausaOpcion
var pausaTimer
var gamePaused = false
var volumen = true



var camara
var camara_area

var vikingoSelec
var actualLevel = null
var tablero = null
var filtro
var pausa
var vikingos
var sound
var main
var item
var enemigos = []


var shakeCamera = false
var shakeTime
var shakeFuerza

var bombaScene
var camara_areaScene
var control

enum State{
	Esperando, Corriendo, Descansando, 
	Cayendo, Empujando, Atontado, Corneando
	Chocando, Escalando, Disparando,
	Atacando, Danio, Muriendo, Teletransportandose
}
enum Direccion{
	QUIETO, UP, DOWN, LEFT, RIGHT
}
enum Escena{
	Nivel, Portal, Mar, Intro
}

var escena_sigue
var tecla_presionada = 0
var solto_tecla = ~0
var just_pressed = 0


func valorBit(position, bytes):
	return bool(bytes & position)

func valerBit(position, valor, unoodos):
	if unoodos:
		if valor == valorBit(position, solto_tecla): return false
		if valor: solto_tecla = solto_tecla | position
		else: solto_tecla ^= solto_tecla & position
		return true
	
	if valor == valorBit(position, tecla_presionada): return false
	if valor: tecla_presionada = tecla_presionada | position
	else: tecla_presionada ^= tecla_presionada & position
	return true

func _get_mask_from_dir(name_dir):
	if name_dir == "Up": return 0
	if name_dir == "Down": return 1
	if name_dir == "Left": return 2
	if name_dir == "Right": return 3

func _is_left_or_right_pressed():
	return Input.is_action_pressed("LEFT") or Input.is_action_pressed("RIGHT")

func _is_left_and_right_pressed():
	return Input.is_action_pressed("LEFT") and Input.is_action_pressed("RIGHT")

func _is_up_or_down_pressed():
	return Input.is_action_pressed("UP") or Input.is_action_pressed("DOWN")

func _is_up_and_down_pressed():
	return Input.is_action_pressed("UP") and Input.is_action_pressed("DOWN")

func redondear(vec):
	var retorno = Vector2(floor(vec.x), floor(vec.y))
	if vec.x - floor(vec.x) > 0.5:
		retorno.x += 1.0
	if vec.y - floor(vec.y) > 0.5:
		retorno.y += 1.0
	return retorno;

func distancia(vec1, vec2):
	return sqrt(pow(vec1.x - vec2.x, 2) + pow(vec1.y - vec2.y,2))

func distancia2(vec1,vec2):
	return pow(vec1.x - vec2.x, 2) + pow(vec1.y - vec2.y,2)

func estaDentro(circuloPos, circuloRadio, point):
	return distancia2(circuloPos,point) <= pow(circuloRadio,2)

func _es_gris(color):
	if abs(color.r - 0.5) > 0.01: return false
	if abs(color.g - 0.5) > 0.01: return false
	if abs(color.b - 0.5) > 0.01: return false
	return true

func _ready():
	pause_mode = PAUSE_MODE_PROCESS
	camara = Camera2D.new()
	camara.current = true
	camara.zoom = Vector2(0.5, 0.5)
	camara.smoothing_enabled = true
	_set_camera_drag_margin(0.0)
	camara.smoothing_speed = 10.0
	bombaScene = preload("res://objetos/Bomba/Bomba.tscn")
	camara_areaScene = preload("res://escenas/camara_area.tscn")
	#control = preload("res://escenas/Control/Control.tscn").instance()
	pausaOpcion = true
	set_physics_process(false)
	set_process(false)
	camara_area = camara_areaScene.instance()
	camara_area.set_meta("tc","cam")
	#add_child(control)
	
	
func _limpiar_level_actual():
	if actualLevel != null:
		actualLevel.remove_child(camara)
		actualLevel.remove_child(camara_area)
		actualLevel.queue_free()
	actualLevel = null


func intercambiar_info_level(vikingosLevel, level):
	vikingos = vikingosLevel
	vikingoSN = 0
	vikingoSelec = _get_vikingo_selec(vikingoSN)
	for v in vikingos.get_children():
		v.z_index = 0
	vikingoSelec.z_index = 1
	
	_limpiar_level_actual()
	
	actualLevel = level
	#camara.smoothing_enabled = true
	camara.position = vikingoSelec.position
	camara.reset_smoothing()
	
	
	camara.limit_left = 0
	camara.limit_top = 0
	camara.limit_right = level.TILES_X * 16
	camara.limit_bottom = level.TILES_Y * 16
	
	level.add_child(camara)
	level.add_child(camara_area)
	set_physics_process(true)
	set_process(true)
	_soltar_teclas()
	
	main.fade_in()
	#filtro._fade_in()
	

func _end_password():
	vikingoSelec._empezar_a_titilar()

func _process(delta):
	if not get_tree().paused:
		totalTime += delta
		if shakeCamera and totalTime > shakeTime:
			shakeCameraEnd()
		
		if Input.is_action_just_pressed("LEFT") and vikingoSelec.MIRA_DERECHA:
			vikingoSelec._set_direccion_mira(false)
		elif Input.is_action_just_pressed("RIGHT") and not vikingoSelec.MIRA_DERECHA:
			vikingoSelec._set_direccion_mira(true)
		elif Input.is_action_pressed("LEFT") and not Input.is_action_pressed("RIGHT") and vikingoSelec.MIRA_DERECHA:
			vikingoSelec._set_direccion_mira(false)
		elif Input.is_action_pressed("RIGHT") and not Input.is_action_pressed("LEFT") and not vikingoSelec.MIRA_DERECHA:
			vikingoSelec._set_direccion_mira(true)
		
		if Input.is_action_just_pressed("TAB"):
			Input.action_release("TAB")
			tablero._iniciar_comercio()
		if main.nivel > 0 and (Input.is_action_just_pressed("ENTER") or Input.is_action_just_pressed("ESC")):
			Input.action_release("ENTER")
			Input.action_release("ESC")
			pausaOpcion = true
			gamePaused = true
			get_tree().paused = true
			pausaTimer = totalTime
			totalTime2 = totalTime
			pausa.show()
			pausa._change_text("¿rendirse?- si   no")
			
		
		camara_area.position = camara.get_camera_screen_center()
	if not gamePaused: return
	totalTime2 += delta
	
	if Input.is_action_just_pressed("ESC"):
		gamePaused = false
		get_tree().paused = false
		pausa.hide()
	
		
	
	if pausaOpcion:
		if Input.is_action_just_pressed("LEFT"):
			Input.action_release("LEFT")
			pausaOpcion = false
			pausaTimer -= 1.0
		if _esperar_tecla():
			gamePaused = false
			get_tree().paused = false
			pausa.hide()
	else:
		if Input.is_action_just_pressed("RIGHT"):
			Input.action_release("RIGHT")
			pausaOpcion = true
			pausaTimer -= 1.0
		if _esperar_tecla():
			gamePaused = false
			get_tree().paused = false
			pausa.hide()
			_salir_del_nivel(Escena.Mar)
		
	if totalTime2 >= pausaTimer + 0.5:
		if pausaOpcion:
			if pausa.texto.find("no") != -1:
				pausa._change_text("¿rendirse?- si   ")
			else:
				pausa._change_text("¿rendirse?- si   no")
		else:
			if pausa.texto.find("si") != -1:
				pausa._change_text("¿rendirse?-      no")
			else:
				pausa._change_text("¿rendirse?- si   no")
		pausaTimer = totalTime2
	



func _physics_process(delta):
	if Input.is_action_just_pressed("NUM"):
		if not get_tree().paused or tablero.pause_mode == PAUSE_MODE_PROCESS:
			if not tablero.state == tablero.StateComercio.Move:
				var vikingoSelecAntes = vikingoSelec
				var apretado
				if Input.is_action_just_pressed("UNO"): apretado = 0
				elif Input.is_action_just_pressed("DOS"): apretado = 1
				else: apretado = 2
				if apretado != vikingoSN:
					var vikingoNuevo = _get_vikingo_selec(apretado)
					if vikingoNuevo != null:
						vikingoSN = apretado
						vikingoSelec = vikingoNuevo
						updateVikingo(vikingoSelecAntes)
				Input.action_release("UNO")
				Input.action_release("DOS")
				Input.action_release("TRES")
		Input.action_release("NUM")
	
	if Input.is_action_just_pressed("Y") and not get_tree().paused:
		_use_object()
		
	
	camara.position = vikingoSelec.position + vikingoSelec.get_offset()
	
	if shakeCamera:
		camara.position.x += rand_range(-shakeFuerza, shakeFuerza)
		camara.position.y += rand_range(-shakeFuerza, shakeFuerza)
	

func cambiar_al_vik_de_la_derecha():
	var vikingoSelecAntes = vikingoSelec
	vikingoSN+=1
	if vikingoSN > 2:
		vikingoSN = 0
	vikingoSelec = _get_vikingo_inPos(true)
	if vikingoSelec == vikingoSelecAntes:
		vikingoSelec = null
		return false
	updateVikingo(vikingoSelecAntes)
	return true

func updateVikingo(vikingoSelecAntes):
	if tablero != null:
		tablero._updateMiniatures()
	for v in vikingos.get_children():
		v.z_index = -1
	if vikingoSelecAntes != null:
		vikingoSelecAntes.z_index = 0
		if not vikingoSelecAntes.get_node("Timer2").is_stopped():
			vikingoSelecAntes._dejar_de_titilar()
		if vikingoSelecAntes.state == State.Empujando:
			vikingoSelecAntes._set_state(State.Descansando)
		if vikingoSelecAntes != vikingoSelec:
			sound.play_sound("menu")
	vikingoSelec.z_index = 1
	vikingoSelec._empezar_a_titilar()

func _get_vikingo_inPos(forRight):
	for i in 3:
		var vikingo = _get_vikingo_selec(vikingoSN)
		if vikingo != null: return vikingo 
		if forRight:
			vikingoSN+=1
			if vikingoSN > 2:
				vikingoSN = 0
		else:
			vikingoSN-=1
			if vikingoSN < 0:
				vikingoSN = 2
	return null
	
func _get_vikingo_selec(num):
	var nombre_vik
	match num:
		0: nombre_vik = "Dave"
		1: nombre_vik = "Skyler"
		2: nombre_vik = "Eddy"
	if not vikingos.has_node(nombre_vik): return null
	return vikingos.get_node(nombre_vik)

func _use_object():
	Input.action_release("Y")
	var miniV = vikingoSelec._get_miniVikingo()
	var exito = false
	var numItem = miniV.items[miniV.itemSelecPos].frame
	match numItem:
		1: 
			_drop_bomb(vikingoSelec.position)
			exito = true
		3, 4, 8: 
			if miniV.HP < 3:
				miniV._vidaSuma(1)
				sound.play_sound("eructo")
				exito = true
		5:
			if miniV.HP < 3:
				miniV._vidaSuma(2)
				sound.play_sound("eructo")
				exito = true
		9:
			exito = miniV._agregar_extra()
			exito = true
		10:
			vikingoSelec._antigravedad()
			exito = true
		7:
			var mueren = []
			for e in enemigos:
				for ec in e.entes_cercanos_monstruo:
					if ec == "cam":
						mueren.push_back(e)
			
			for m in mueren:
				m._death()
			
			sound.play_sound("explotatodo")
			filtro._activar_filtro_blanco()
			exito = true
		2, 6:#2 llave roja, 6 llave amarilla
			if not vikingoSelec.has_meta("cerr"): return
			var cerradura = vikingoSelec.get_meta("cerr")
			if (numItem == 2 and cerradura.get_meta("c") != "Rojo") or (numItem == 6 and cerradura.get_meta("c") != "Amarillo"): return
			cerradura._abrir()
			vikingoSelec.set_meta("cerr",null)
			exito = true
	
	if not exito: 
		sound.play_sound("no")
		return
	miniV.items[miniV.itemSelecPos].frame = 0
	
	if not miniV._esta_vacio():
		for i in 4:
			if miniV.items[i].frame != 0:
				miniV.itemSelecPos = i
				if not main.android:
					miniV.actualizarSelec()
				break
	
	

func shakeCameraBegin(time = 0.5, motion = 15):
	shakeCamera = true
	shakeTime = totalTime + time
	shakeFuerza = motion

func shakeCameraEnd():
	shakeCamera = false

func _set_camera_drag_margin(valor):
	camara.drag_margin_bottom = valor
	camara.drag_margin_right = valor
	camara.drag_margin_top = valor
	camara.drag_margin_left = valor

func _drop_bomb(position):
	sound.play_sound("globo")
	var bomba = bombaScene.instance()
	bomba.position = position
	actualLevel.add_child(bomba)

func _get_mini(nameVikingo):
	if tablero == null: return null
	return tablero.get_node(nameVikingo)

func _esperar_tecla():
	if Input.is_action_just_pressed("A"):
		Input.action_release("A")
		return true
	if Input.is_action_just_pressed("B"):
		Input.action_release("B")
		return true
	if Input.is_action_just_pressed("C"):
		Input.action_release("C")
		return true
	if Input.is_action_just_pressed("Y"):
		Input.action_release("Y")
		return true
	if Input.is_action_just_pressed("ENTER"):
		Input.action_release("ENTER")
		return true
	return false

func _soltar_teclas():
	Input.action_release("A")
	Input.action_release("B")
	Input.action_release("C")
	Input.action_release("Y")
	Input.action_release("Enter")
	Input.action_release("NUM")
	Input.action_release("UNO")
	Input.action_release("DOS")
	Input.action_release("TRES")
	Input.action_release("TAB")
	Input.action_release("UP")
	Input.action_release("DOWN")
	Input.action_release("LEFT")
	Input.action_release("RIGHT")

func _salir_del_nivel(escena_siguiente):
	escena_sigue = escena_siguiente
	set_physics_process(false)
	set_process(false)
	get_tree().paused = true
	main.fade_out()
	












