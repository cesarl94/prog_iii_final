extends Sprite

export(String,"Carne","Cebolla","Manzana","Bomba","Pollo","Extra","Explotatodo","LlaveAmarilla","LlaveRoja","Botas") var TC_NAME = null setget setName

onready var Handle = get_node("/root/Handle")
var vikingos = []
var numItem = 0

func setName(newVar):
	TC_NAME = newVar

func _ready():
	set_process(false)
	match TC_NAME:
		"Carne": numItem = 5
		"Cebolla": numItem = 4
		"Manzana": numItem = 8
		"Bomba": numItem = 1
		"Pollo": numItem = 3
		"Extra": numItem = 9
		"Explotatodo": numItem = 7
		"LlaveAmarilla": numItem = 6
		"LlaveRoja": numItem = 2
		"Botas": numItem = 10

func _process(delta):
	for v in vikingos:
		if Handle.tablero._meterItem(v.get_node("Area2D").get_meta("v"), numItem):
			queue_free()

func _on_area_entered(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	if Handle.tablero == null or Handle.tablero._meterItem(area.get_meta("v"), numItem):
		queue_free()
		if Handle.tablero == null:
			Handle.sound.play_sound("agarra_item")
	else: 
		vikingos.push_back(area.get_meta("p"))
		set_process(true)

func _on_area_exited(area):
	if not area.has_meta("tc") or area.get_meta("tc") != "v": return
	vikingos.erase(area.get_meta("p"))
	if vikingos.empty():
		set_process(false)
