extends TileMap

class dataTile : 
	var IDglobal
	var IDlocal
	var duration
	var frames = []
	func getTilesAvanza():
		var valor = frames.front()
		var comenzo = false
		for f in frames:
			if not comenzo:
				comenzo = true
			else:
				return f - valor


class animations:
	var firstgid
	var tileSize
	var tilesEnX
	var anchoImagen
	var altoImagen
	var dataTiles = []
	func getPosOfTile(numTile):
		var retorno = Vector2()
		var posX = numTile % tilesEnX
		var posY = numTile / tilesEnX
		retorno.x = posX * tileSize
		retorno.y = posY * tileSize
		return retorno
	

var cargado = false

func _cargar_tiles_animados():
	var nombreLevel = _get_nombre_level()
	var path = "res://levels/" + nombreLevel + "/" + nombreLevel + ".dta"
	var shaderAnimated = load("res://effects/animatedTiles/animatedLayer.material")
	var file = File.new()
	if(!file.file_exists(path)):
		return
	
	file.open(path,file.READ)
	
	var animaciones = []
	var cantidadAnimaciones = file.get_32()
	for i in cantidadAnimaciones:
		var animacion = animations.new()
		animacion.firstgid = file.get_32()
		animacion.tileSize = file.get_32()
		animacion.tilesEnX = file.get_32()
		animacion.anchoImagen = file.get_32()
		animacion.altoImagen = file.get_32()
		var cantidadTilesAnimados = file.get_32()
		for j in cantidadTilesAnimados:
			var tileAnimado = dataTile.new()
			tileAnimado.IDglobal = file.get_32()
			tileAnimado.IDlocal = file.get_32()
			tileAnimado.duration = file.get_32()
			var cantidadDeFrames = file.get_32()
			for k in cantidadDeFrames:
				tileAnimado.frames.push_back(file.get_32())
			
			tile_set.tile_set_material(tileAnimado.IDglobal, shaderAnimated)
			var material_ = tile_set.tile_get_material(tileAnimado.IDglobal)
			material_.set_shader_param("cantFrames",tileAnimado.frames.size())
			material_.set_shader_param("duracion",tileAnimado.duration / 1000.0)
			material_.set_shader_param("tilesAvanza",tileAnimado.getTilesAvanza())
			var tileSize = Vector2(animacion.tileSize, animacion.tileSize)
			var imagenSize = Vector2(animacion.anchoImagen, animacion.altoImagen)
			material_.set_shader_param("tileSizeNormalized",Vector2(tileSize / imagenSize))
			material_.set_shader_param("columnas",animacion.tilesEnX)
	
	file.close()
	cargado = true

func _get_nombre_level():
	var path = String(get_path())
	var retorno = ""
	var ultima_palabra
	for c in path:
		if c == '/':
			ultima_palabra = retorno
			retorno.erase(0,retorno.length())
		else:
			retorno += c
	return ultima_palabra
