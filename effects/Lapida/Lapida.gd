extends Sprite

export (PackedScene) var Poof = null

func _on_animation_finished(anim_name):
	if Poof != null:
		var puflito = Poof.instance()
		puflito.position = global_position
		get_parent().add_child(puflito)
		Handle.sound.play_sound("lapida")
	queue_free()
