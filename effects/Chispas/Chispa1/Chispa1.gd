extends Sprite

var extenderTiempo

func _on_animation_finished(anim_name):
	if extenderTiempo < 0.05:
		queue_free()
	else:
		$Timer.wait_time = extenderTiempo
		$Timer.start()

func _play(newPosition, flip = false, forward = true, tiempo_extra = 0.0):
	extenderTiempo = tiempo_extra
	position = newPosition
	flip_h = flip
	z_index = 9.1
	if forward: $AnimationPlayer.play("Forward")
	else: $AnimationPlayer.play("Backward")
	

func _on_Timer_timeout():
	queue_free()
