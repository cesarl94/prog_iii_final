extends Node2D

export(PackedScene) var Escena_humo = null
export(PackedScene) var Escena_lapida = null

func _activar(posicion):
	position = posicion
	if Escena_humo != null:
		var humo = Escena_humo.instance()
		humo.position = position
		get_parent().add_child(humo)
	$Timer.start()
	Handle.sound.play_sound("poof")

func _on_Timer_timeout():
	if Escena_lapida != null:
		var lapida = Escena_lapida.instance()
		lapida.position = position
		get_parent().add_child(lapida)
	queue_free()
